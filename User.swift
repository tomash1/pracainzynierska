//
//  User.swift
//  Praca Inzynierska
//
//  Created by Tomasz Domaracki on 05.10.2015.
//  Copyright © 2015 Tomek Domaracki. All rights reserved.
//

import UIKit
import ObjectMapper

class User : Mappable {
    
    var id : Int?
    var email : String?
    var passwordHash : String?
    var beacons : Int?
    var notifications : Int?
    
    init() {}
    
    required init?(_ map: Map) {}
    
    func mapping(map: Map) {
        id              <- map["id"]
        email           <- map["email"]
        passwordHash    <- map["passwordHash"]
        beacons         <- map["beacons"]
        notifications   <- map["notifications"]
    }
    
    func toJSON() -> String {
        if let jsonUser = Mapper<User>().toJSONString(self, prettyPrint: false){
            return jsonUser
        }
        return ""
    }
    
    static func fromJSON(data: NSData?) -> User? {
        if let userData = data {
            let dataString = NSString(data: userData, encoding: NSUTF8StringEncoding) as! String
            if let user = Mapper<User>().map(dataString) {
                return user
            }
        }
        return nil
    }
}
