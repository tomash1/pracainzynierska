//
//  Device.swift
//  Praca Inzynierska
//
//  Created by Tomek Domaracki on 02.12.2015.
//  Copyright © 2015 Tomek Domaracki. All rights reserved.
//

import UIKit
import ObjectMapper

class Device: Mappable {
    
    var id: Int?
    var deviceId: String?
    var notificationPriority: Int?
    var userId: Int?
    
    init() {}
    required init?(_ map: Map) {}
    
    func mapping(map: Map) {
        id                   <- map["id"]
        deviceId             <- map["deviceId"]
        notificationPriority <- map["notificationPriority"]
        userId               <- map["userId"]
    }
    
    func toJSON() -> String {
        if let jsonBeacon = Mapper<Device>().toJSONString(self, prettyPrint: false){
            return jsonBeacon
        }
        return ""
    }
    
    static func fromJSON(data: NSData?) -> Device? {
        if let deviceData = data {
            let dataString = NSString(data: deviceData, encoding: NSUTF8StringEncoding) as! String
            if let device = Mapper<Device>().map(dataString) {
                return device
            }
        }
        return nil
    }
}
