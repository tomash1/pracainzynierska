# README #
__Reason of creation__

The program has been created as a project to get a BSc degree in Informatics on Silesian University of Technology.

__Short description__

The PracaInzynierska(FindStuff) app localize stuff using beacons. Two levels are supported:

* ** nearby **
e.g. when forgetting notebook from home
* ** worldwide **
e.g. when luggage at the airport is lost (nedded other users of system)

__Screenshots__

![IMG_0073.PNG](https://bitbucket.org/repo/n57BLg/images/2265424207-IMG_0073.PNG)
![IMG_0069.PNG](https://bitbucket.org/repo/n57BLg/images/3061702060-IMG_0069.PNG)
![IMG_0067.PNG](https://bitbucket.org/repo/n57BLg/images/724744858-IMG_0067.PNG)
![IMG_0062.PNG](https://bitbucket.org/repo/n57BLg/images/3422587405-IMG_0062.PNG)