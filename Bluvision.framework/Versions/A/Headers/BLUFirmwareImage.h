//
//  BLUFirmwareImage.h
//
//  Copyright (c) 2015 BluVision. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

/**
 *  `BLUFirmwareImage` objects are used to update a beacon's firmware.
 *
 *  @discussion A firmware image instance can be created from any valid firmware file provided as NSData.
 *  The firmware image object can be used to start a firmware update by calling `updateFirmwareWithImage:progress:completion:`
 *  on a `BLUConfigurableBeacon` instance.
 */
@interface BLUFirmwareImage : NSObject

#pragma mark - Properties
/** @name Properties*/

/**
 *  The size of the firmware image in bytes.
 */
@property (nonatomic, readonly) NSNumber *size;

/**
 *  The revision of the firmware image.
 */
@property (nonatomic, readonly) NSNumber *revision;

#pragma mark - Initialization
/** @name Initialization */

- (instancetype)init UNAVAILABLE_ATTRIBUTE;

/**
 *  Initializes an instance of BLUFirmwareImage with the provided firmware data
 *
 *  @param firmwareData  An NSData object containing the firmware update image.
 *
 *  @return Returns a newly initialized BLUFirmwareImage instance.
 */
- (instancetype)initWithData:(NSData *)firmwareData NS_DESIGNATED_INITIALIZER;

/**
 *  Reads a firmware file provided as NSData and returns its firmware revision.
 *
 *  @param firmwareData  An NSData object containing the firmware update image.
 *
 *  @return Returns the firmware revision of the specified data or nil if the data is not compatible.
 */
+ (NSNumber *)firmwareRevisionForData:(NSData *)firmwareData;

@end

NS_ASSUME_NONNULL_END
