//
//  BLUIBeacon.h
//
//  Copyright (c) 2015 Bluvision. All rights reserved.
//

#import "BLUBeacon.h"

NS_ASSUME_NONNULL_BEGIN
/**
 *  Defines the accuracy value of CoreLocation's iBeacon ranging events.
 *  Used as a light wrapper around CLLocationAccuracy.
 *  For more information on iBeacon ranging see Apple's CoreLocation documentation.
 */
typedef double BLULocationAccuracy;

/**
 *  `BLUIBeacon` objects represent an Apple iBeacon.
 *
 *  @discussion A `BLUIBeacon` can be used in two different ways:
 *  It can represent a light wrapper around a CoreLocation CLBeacon object during a scan for iBeacons.
 *  It can be used to create an iBeacon configuration for setting up a configurable beacon.
 */
@interface BLUIBeacon : BLUBeacon

#pragma mark - Properties
/** @name properties */

/**
 *  The iBeacon's proximity UUID.
 */
@property (nonatomic, readonly) NSUUID *proximityUUID;

/**
 *  The iBeacon's major value.
 */
@property (nonatomic, readonly) NSNumber *major;

/**
 *  The iBeacon's minor value.
 */
@property (nonatomic, readonly) NSNumber *minor;

/**
 *  The accuracy of the last received iBeacon ranging event.
 */
@property (nonatomic, readonly) BLULocationAccuracy accuracy;

#pragma mark - Initalization
/** @name Initialization*/

/**
 *  Initializes a `BLUIBeacon` instance with a proximity UUID, a major and a minor value.
 *
 *  @param proximityUUID The iBeacon's proximity UUID.
 *  @param major         The iBeacon's major value.
 *  @param minor         The iBeacon's minor value.
 *
 *  @return Returns a newly initialized `BLUIBeacon` instance.
 */
- (instancetype)initWithProximityUUID:(NSUUID *)proximityUUID major:(NSNumber *)major minor:(NSNumber *)minor;

@end

NS_ASSUME_NONNULL_END
