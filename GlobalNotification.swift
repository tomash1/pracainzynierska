//
//  GlobalNotification.swift
//  Praca Inzynierska
//
//  Created by Tomek Domaracki on 16.11.2015.
//  Copyright © 2015 Tomek Domaracki. All rights reserved.
//

import UIKit
import ObjectMapper

class GlobalNotification: Mappable {
    
    var message : String?
    var beacon : Beacon?
    
    init() {}
    required init?(_ map: Map) {}
    
    func mapping(map: Map) {
        message                 <- map["message"]
        beacon                  <- map["beacon"]
    }

    func toJSON() -> String {
        if let jsonNotification = Mapper<GlobalNotification>().toJSONString(self, prettyPrint: false){
            return jsonNotification
        }
        return ""
    }
    
    
    static func fromJSON(data: NSData?) -> GlobalNotification? {
        if let notificationData = data {
            let dataString = NSString(data: notificationData, encoding: NSUTF8StringEncoding) as! String
            if let notification = Mapper<GlobalNotification>().map(dataString) {
                return notification
            }
        }
        return nil
    }
}
