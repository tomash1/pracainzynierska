//
//  Animator.swift
//  Praca Inzynierska
//
//  Created by Tomek Domaracki on 05.07.2015.
//  Copyright (c) 2015 Tomek Domaracki. All rights reserved.
//

import UIKit

class Animator: NSObject, UIViewControllerAnimatedTransitioning, UIViewControllerTransitioningDelegate {
    
    var presenting = true
    
    func animateTextFieldError(toAnimate : UITextField){
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.07
        animation.repeatCount = 4
        animation.autoreverses = true
        animation.fromValue = NSValue(CGPoint: CGPointMake(toAnimate.center.x, toAnimate.center.y))
        animation.toValue = NSValue(CGPoint: CGPointMake(toAnimate.center.x + 20, toAnimate.center.y))
        toAnimate.layer.addAnimation(animation, forKey: "position")
    }
    
    func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
        
        let container = transitionContext.containerView()
        let fromView  = transitionContext.viewForKey(UITransitionContextFromViewKey)!
        let toView    = transitionContext.viewForKey(UITransitionContextToViewKey)!
        
        let offScreenRight = CGAffineTransformMakeTranslation(container!.frame.width, 0)
        let offScreenLeft  = CGAffineTransformMakeTranslation(-container!.frame.width, 0)
        
        container!.backgroundColor = UIColor(patternImage: UIImage(named: "appBackground.jpg")!)
        
        toView.transform = self.presenting ? offScreenRight : offScreenLeft
        
        container!.addSubview(toView)
        container!.addSubview(fromView)
        
        let duration = self.transitionDuration(transitionContext)
        
        UIView.animateWithDuration(duration, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.8, options: [], animations: { () -> Void in
            fromView.transform = self.presenting ? offScreenLeft : offScreenRight
            toView.transform = CGAffineTransformIdentity
            }, completion: { finished in transitionContext.completeTransition(true) })
        
    }
    
    func transitionDuration(transitionContext: UIViewControllerContextTransitioning?) -> NSTimeInterval {
        return 0.5
    }
    
    func animationControllerForPresentedController(presented: UIViewController, presentingController presenting: UIViewController, sourceController source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        self.presenting = true
        return self
    }
    
    func animationControllerForDismissedController(dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        self.presenting = false
        return self
    }
}
