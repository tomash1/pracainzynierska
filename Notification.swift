//
//  Notification.swift
//  Praca Inzynierska
//
//  Created by Tomasz Domaracki on 05.10.2015.
//  Copyright © 2015 Tomek Domaracki. All rights reserved.
//

import UIKit
import ObjectMapper

class Notification: Mappable {
    
    var id : Int?
    var geoCoordinatesLatitude : Double?
    var geoCoordinatesLongitude : Double?
    var message : String?
    var userId : Int?
    var timestamp: String?

    init() {}
    required init?(_ map: Map) {}
    
    func mapping(map: Map) {
        id                      <- map["id"]
        geoCoordinatesLatitude  <- map["geoCoordinatesLatitude"]
        geoCoordinatesLongitude <- map["geoCoordinatesLongitude"]
        message                 <- map["message"]
        timestamp               <- map["notificationTime"]
        userId                  <- map["userId"]
    }
    
    func toJSON() -> String {
        if let jsonNotification = Mapper<Notification>().toJSONString(self, prettyPrint: true){
            return jsonNotification
        }
        return ""
    }
    
    static func fromJSON(data: NSData?) -> Notification? {
        if let notificationData = data {
            let dataString = NSString(data: notificationData, encoding: NSUTF8StringEncoding) as! String
            if let notification = Mapper<Notification>().map(dataString) {
                return notification
            }
        }
        return nil
    }
}
