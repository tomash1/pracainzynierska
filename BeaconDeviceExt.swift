//
//  BeaconDevice.swift
//  Praca Inzynierska
//
//  Created by Tomasz Domaracki on 18.10.2015.
//  Copyright © 2015 Tomek Domaracki. All rights reserved.
//

import UIKit
import ObjectMapper

class BeaconDeviceExt: Beacon {
    let retryCount: UInt = 3
    let timeout = 20.0
    
    var BeaconDevice: BLUConfigurableBeacon!
    
    var deviceName  : String!
    var beaconName  : String!

    var status = BeaconAvailability.Unknown
    
    var signalStrength      = -100
    var notificationsCount  = 0
    
    init(beacon: BLUConfigurableBeacon) {
        BeaconDevice = beacon
        deviceName   = BLUBeaconExt(bluBeacon: BeaconDevice).getBeaconName()
        super.init()
    }
    
    init(beacon: BeaconDeviceExt){
        BeaconDevice = beacon.BeaconDevice
        deviceName   = BLUBeaconExt(bluBeacon: BeaconDevice).getBeaconName()
        super.init()
    }
 
    init(beacon: Beacon){
        super.init()
        self.name     = beacon.name
        self.minorId  = beacon.minorId
        self.majorId  = beacon.majorId
        self.uuid     = beacon.uuid
        self.id       = beacon.id
        self.userId   = beacon.userId
        self.deviceId = beacon.deviceId
    }
    
    required init?(_ map: Map) {
        super.init(map)
    }
    
    func connect() {
        BeaconDevice.connectWithPassword(nil, timeoutInterval: timeout, retryCount: retryCount)
    }
    
    func connectWithPassword(password: String) {
        BeaconDevice.connectWithPassword(password, timeoutInterval: timeout, retryCount: retryCount)
    }
    
    func isConnected() -> Bool {
        if BeaconDevice != nil {
            return BeaconDevice.connectionState == BLUConfigurableBeaconConnectionState.Connected
        }
        return false
    }
}
