//
//  FoundNotification.swift
//  Praca Inzynierska
//
//  Created by Tomek Domaracki on 30.11.2015.
//  Copyright © 2015 Tomek Domaracki. All rights reserved.
//

import UIKit
import ObjectMapper

class FoundNotification: Mappable {
    
    var uuid : String?
    var majorId : String?
    var minorId : String?
    var notification : Notification?
    
    init() {}
    required init?(_ map: Map) {}
    
    func mapping(map: Map) {
        uuid                 <- map["uuid"]
        majorId              <- map["majorId"]
        minorId              <- map["minorId"]
        notification         <- map["notification"]
    }
    
    func toJSON() -> String {
        if let jsonNotification = Mapper<FoundNotification>().toJSONString(self, prettyPrint: false){
            return jsonNotification
        }
        return ""
    }
    
    
    static func fromJSON(data: NSData?) -> FoundNotification? {
        if let notificationData = data {
            let dataString = NSString(data: notificationData, encoding: NSUTF8StringEncoding) as! String
            if let notification = Mapper<FoundNotification>().map(dataString) {
                return notification
            }
        }
        return nil
    }

}
