//
//  Beacon.swift
//  Praca Inzynierska
//
//  Created by Tomasz Domaracki on 05.10.2015.
//  Copyright © 2015 Tomek Domaracki. All rights reserved.
//

import UIKit
import ObjectMapper

class Beacon: Mappable {
    
    var id: Int?
    var deviceId: Int?
    var majorId: String?
    var minorId: String?
    var uuid: String?
    var name: String?
    var userId: Int?
    
    init() {}
    required init?(_ map: Map) {}
    
    func mapping(map: Map) {
        id              <- map["id"]
        deviceId        <- map["deviceId"]
        majorId         <- map["majorId"]
        minorId         <- map["minorId"]
        uuid            <- map["uuid"]
        name            <- map["name"]
        userId          <- map["userId"]
    }
    
    func toJSON() -> String {
        if let jsonBeacon = Mapper<Beacon>().toJSONString(self, prettyPrint: false){
            return jsonBeacon
        }
        return ""
    }
    
    static func fromJSON(data: NSData?) -> Beacon? {
        if let beaconData = data {
            let dataString = NSString(data: beaconData, encoding: NSUTF8StringEncoding) as! String
            if let beacon = Mapper<Beacon>().map(dataString) {
                return beacon
            }
        }
        return nil
    }
}
