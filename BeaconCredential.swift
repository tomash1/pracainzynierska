//
//  BeaconCredential.swift
//  Praca Inzynierska
//
//  Created by Tomasz Domaracki on 18.10.2015.
//  Copyright © 2015 Tomek Domaracki. All rights reserved.
//

import UIKit
import ObjectMapper

class BeaconCredential: Mappable {
    
    var majorId: String?
    var minorId: String?
    var uuid: String?
    
    init() {}
    required init?(_ map: Map) {}
    
    func mapping(map: Map) {
        majorId         <- map["majorId"]
        minorId         <- map["minorId"]
        uuid            <- map["uuid"]
    }
    
    func toJSON() -> String {
        if let jsonBeacon = Mapper<BeaconCredential>().toJSONString(self, prettyPrint: false){
            return jsonBeacon
        }
        return ""
    }
    
    static func fromJSON(data: NSData?) -> BeaconCredential? {
        if let beaconCredentialData = data {
            let dataString = NSString(data: beaconCredentialData, encoding: NSUTF8StringEncoding) as! String
            if let beaconCredential = Mapper<BeaconCredential>().map(dataString) {
                return beaconCredential
            }
        }
        return nil
    }

}
