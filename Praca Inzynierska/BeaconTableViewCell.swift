//
//  BeaconTableViewCell.swift
//  Praca Inzynierska
//
//  Created by Tomasz Domaracki on 09.10.2015.
//  Copyright © 2015 Tomek Domaracki. All rights reserved.
//

import UIKit

class BeaconTableViewCell: UITableViewCell {

    @IBOutlet weak var beaconNameLabel: UILabel!
    @IBOutlet weak var beaconSignalStrengthValueLabel: UILabel!
    
    
    func withTitle(title: String) -> BeaconTableViewCell {
        beaconNameLabel.text = title
        return self
    }
    
    func withRssiValue(value: String) -> BeaconTableViewCell {
        beaconSignalStrengthValueLabel.text = value + " dBm"
        return self
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
