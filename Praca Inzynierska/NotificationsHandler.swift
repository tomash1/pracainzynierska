//
//  NotificationsHandler.swift
//  Praca Inzynierska
//
//  Created by Tomek Domaracki on 09.11.2015.
//  Copyright © 2015 Tomek Domaracki. All rights reserved.
//

import UIKit
import SwiftyJSON

class NotificationsHandler: NSObject {
    
//    let enteredBeaconRegion: CLBeaconRegion
    var isEnterNotification: Bool?
    var userRegions: Array<CLBeaconRegion> = Array()
    let local = LocalNotificationsHandler()
    let global = GlobalNotificationsHandler()
    
    func execute(enteredBeaconRegion: CLBeaconRegion, isEnterNotification: Bool) {
        let fileManager = BeaconRegionsFileManager()
        userRegions = fileManager.getRegions()
        generateNotitfication(enteredBeaconRegion, isEnterNotification: isEnterNotification)
    }
    
    private func generateNotitfication(enteredBeaconRegion: CLBeaconRegion, isEnterNotification: Bool){
        self.isEnterNotification = isEnterNotification
        var isLocal = false
        for region in userRegions {
            if region == enteredBeaconRegion {
                isLocal = true
                break
            }
        }
        
        if isLocal {
            local.handleNotification(isEnterNotification)
        } else {
            global.handleNotification(enteredBeaconRegion, isEnterNotification: isEnterNotification)
        }
    }
    
    func handleGlobalNotificationsResponse(result: NSData?) {
        if let notificationsArrayRaw = result {
            let notificationsArrayJSON = JSON(data: notificationsArrayRaw, options: NSJSONReadingOptions.MutableContainers)
            let notificationsArray = JSONTranslator.convertJsonToNotificationsArray(notificationsArrayJSON)
            var globalRegions = Array<CLBeaconRegion>()
            
            let finder = IBeaconsFinder()
            for notification in notificationsArray {
                if let notificationBeacon = notification.beacon{
                    let beaconExt = BeaconExt(beacon: notificationBeacon)
                    if let region = beaconExt.getBeaconRegion() {
                        if(!finder.lookingfForRegion(region)){
                            finder.startRangingBeaconsInRegion(region)
                        }
                        globalRegions.append(region)
                    }
                }
            }
            let removed = getDifferenceInRegions(finder.getRegions(), globalRegions: globalRegions)

            for region in removed {
                finder.stopRangingBeaconsInRegion(region)
            }
        }
    }
    
    private func getDifferenceInRegions(deviceRegions: Set<CLRegion>, globalRegions: Array<CLBeaconRegion>) -> Array<CLBeaconRegion>{
        var differences = Array<CLBeaconRegion>()
        for deviceRegion in deviceRegions {
            if let region = deviceRegion as? CLBeaconRegion {
                print(region.proximityUUID)
                if(!globalRegions.contains(region)){
                    differences.append(region)
                }
            }
        }
        return differences
    }
}
