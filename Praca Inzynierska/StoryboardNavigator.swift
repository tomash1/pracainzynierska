//
//  StoryboardNavigator.swift
//  Praca Inzynierska
//
//  Created by Tomek Domaracki on 30.06.2015.
//  Copyright (c) 2015 Tomek Domaracki. All rights reserved.
//

import UIKit

class StoryboardNavigator: NSObject {
   
    let StoryBoardName : String
    
    init(storyboardName : String) {
        self.StoryBoardName = storyboardName
    }
    
    func moveToStoryboard(currentViewController : UIViewController) {
        let storyboard = UIStoryboard(name: StoryBoardName, bundle: nil)
        let vc: AnyObject = storyboard.instantiateInitialViewController()!
        currentViewController.presentViewController(vc as! UIViewController, animated: true, completion: nil)
    }
}
