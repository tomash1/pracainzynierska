//
//  CreateAccountViewController.swift
//  Praca Inzynierska
//
//  Created by Tomek Domaracki on 28.06.2015.
//  Copyright (c) 2015 Tomek Domaracki. All rights reserved.
//

import UIKit

class CreateAccountUserViewController: UIViewController, UITextFieldDelegate {
    

    @IBOutlet var ContinueButton: UIButton!
    @IBOutlet var EmailTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let continueButtonLook = ButtonLook(fromUIButton: ContinueButton)
        continueButtonLook.addRadiusToButton()
        
        let navBarLook = NavigationBarLook(controller: self.navigationController!)
        navBarLook.setLook()
        navBarLook.addLogoToBarTitle(navigationItem)
        
        EmailTextField.delegate = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        view.endEditing(true)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "createPasswordSegue"){
            let createPasswordViewController = segue.destinationViewController as! CreateAccountPasswordViewController
            
            let user = User()
            user.email = EmailTextField.text
            createPasswordViewController.user = user
        }
    }
    
    override func shouldPerformSegueWithIdentifier(identifier: String, sender: AnyObject?) -> Bool {
        if(identifier == "createPasswordSegue"){
            if(EmailTextField.text == "" || !isValidEmail(EmailTextField.text)){
                Animator().animateTextFieldError(EmailTextField)
                return false
            }
        }
        return true
    }
    
    private  func isValidEmail(testStr:String?) -> Bool {
        if let testStr = testStr {
            let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
            
            let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
            return emailTest.evaluateWithObject(testStr)
        }
        return false
    }
}
