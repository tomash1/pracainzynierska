//
//  ChangePasswordViewController.swift
//  Praca Inzynierska
//
//  Created by Tomasz Domaracki on 02.10.2015.
//  Copyright © 2015 Tomek Domaracki. All rights reserved.
//

import UIKit

class ChangePasswordViewController: UIViewController {

    @IBOutlet weak var ChangePasswordButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        let buttonLook = ButtonLook(fromUIButton: ChangePasswordButton)
        buttonLook.addRadiusToButton()
        buttonLook.addBorderToButton()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
