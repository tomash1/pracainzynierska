//
//  ButtonLook.swift
//  Praca Inzynierska
//
//  Created by Tomek Domaracki on 30.06.2015.
//  Copyright (c) 2015 Tomek Domaracki. All rights reserved.
//

import UIKit

class ButtonLook: NSObject {
    
    let button : UIButton
    
    init (fromUIButton button : UIButton) {
        self.button = button
    }
    
    func addBorderToButton () {
        button.layer.cornerRadius = 3
        button.layer.borderWidth = 0.5
        button.layer.borderColor = UIColor.whiteColor().CGColor
    }

    func addRadiusToButton () {
        button.layer.cornerRadius = 3
    }
    
}
