//
//  POSTSender.swift
//  Praca Inzynierska
//
//  Created by Tomasz Domaracki on 01.10.2015.
//  Copyright © 2015 Tomek Domaracki. All rights reserved.
//

import UIKit
import ObjectMapper
import SwiftyJSON

class POSTSender: RequestSender {
    
    let base64Converter = Base64Converter()
    var toAddAddress = ""
    var postData : NSData?
    
    func execute(responseData: (result: NSData?) -> Void) {
        let address = REST_SERVICE_ADDRESS + toAddAddress
        
        request = NSMutableURLRequest(URL: NSURL(string: address)!)
        request!.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request!.setValue("application/json", forHTTPHeaderField: "Accept")
        request!.HTTPMethod = "POST"
        request!.HTTPBody = postData
        
        addTokenToRequest()
        
        self.send(responseData)
    }
    
    func addUser(userToAdd : User) -> POSTSender {
        toAddAddress = "/users"
        postData = userToAdd.toJSON().dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)
        return self
    }
    
    func addDevice(deviceToAdd: Device) -> POSTSender {
        toAddAddress = "/devices"
        if let userEmail = UserCredentialsManager.getUserName() {
            toAddAddress += "/" + userEmail
        }
        postData = deviceToAdd.toJSON().dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)
        
        return self

    }
    
    func addBeacon(beaconToAdd : Beacon) -> POSTSender {
        toAddAddress = "/beacons"
        if let userEmail = UserCredentialsManager.getUserName() {
            toAddAddress += "/" + userEmail
        }
        postData = beaconToAdd.toJSON().dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)
        
        return self
    }
    
    func addFoundNotification(foundNotification: FoundNotification) -> POSTSender {
        toAddAddress = "/notifications"
        
        postData = foundNotification.toJSON().dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)
        return self
    }
    
    func addGlobalNotification(beaconNotification: BeaconExt) -> POSTSender{
        toAddAddress = "/devicenotifications"

        let notification = GlobalNotification()
        notification.message = "My first notification"
        notification.beacon = beaconNotification._Beacon
        postData = notification.toJSON().dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)
        
        return self
    }
    
    func tempFoundNotification() -> POSTSender{
        toAddAddress = "/devicenotifications"
        if let userEmail = UserCredentialsManager.getUserName() {
            toAddAddress += "/" + userEmail
        }
        
        postData = "found".dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)
        
        return self
    }
}
