//
//  UserCredentialsManager.swift
//  Praca Inzynierska
//
//  Created by Tomek Domaracki on 24.11.2015.
//  Copyright © 2015 Tomek Domaracki. All rights reserved.
//

import UIKit

class UserCredentialsManager: NSObject {
    private static let USER_NAME = "username"
    private static let USER_ID   = "userid"
    
    static let keychainWrapper = KeychainWrapper()
    static let userDefaults    = NSUserDefaults.standardUserDefaults()
    
    static func setUserName(userName: String){
        userDefaults.setValue(userName, forKey: USER_NAME)
        userDefaults.synchronize()
    }
    
    static func getUserName() -> String? {
        return userDefaults.valueForKey(USER_NAME) as? String
    }
    
    static func setUserToken(token: String){
        writeToKeyChain(token)
    }
    
    static func setUserNSToken(token: NSString){
        let token = token as String
        writeToKeyChain(token)
    }
    
    static func getToken() -> String? {
        if let username = getUserName() {
            keychainWrapper.mySetObject(username, forKey: kSecAttrAccount)
            return keychainWrapper.myObjectForKey(kSecValueData) as? String
        }
        return nil
    }
    
    static func removeUserData(){
        userDefaults.removeObjectForKey(USER_ID)
        userDefaults.removeObjectForKey(USER_NAME)
    }
    
    static func setUserId(userId: Int) {
        let userIdS = String( userId )
        userDefaults.setValue(userIdS, forKey: USER_ID)
    }
    
    static func getUserId() -> String? {
        let userId = userDefaults.valueForKey(USER_ID) as? String
        return userId
    }
    
    static func userLoggedPreviously() -> Bool {
        if let _ = getUserName() {
            if let _ = getUserId() {
                return true
            }
        }
        return false
    }
    
    private static func writeToKeyChain(authorizationToken: String) {
        if let username = getUserName() {
            keychainWrapper.mySetObject("FIND_STUFF", forKey: kSecAttrService)
            keychainWrapper.mySetObject(username, forKey: kSecAttrAccount)
            keychainWrapper.mySetObject(authorizationToken, forKey: kSecValueData)
            keychainWrapper.writeToKeychain()
            
            userDefaults.synchronize()
        }
    }
}
