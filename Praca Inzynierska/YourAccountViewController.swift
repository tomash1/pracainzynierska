//
//  YourAccountViewController.swift
//  Praca Inzynierska
//
//  Created by Tomasz Domaracki on 02.10.2015.
//  Copyright © 2015 Tomek Domaracki. All rights reserved.
//

import UIKit
import SwiftSpinner

class YourAccountViewController: UIViewController, UserNotifierDelegate {

    @IBOutlet weak var DeleteAccountButton: UIButton!
    @IBOutlet weak var ManagePasswordButton: UIButton!
    @IBOutlet weak var UserEmailLabel: UILabel!
    
    let DELETE_CONFIRMATION_MESSAGE = "Account Delete Warning".localized
    let WARNING_AGREE_MESSAGE       = "Yes".localized
    let WARNING_CANCEL_MESSAGE      = "No".localized
    
    var userNotifier: UserNotifier!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let deleteAccountLook = ButtonLook(fromUIButton: DeleteAccountButton)
        deleteAccountLook.addRadiusToButton()
        
        let passwordButtonLook = ButtonLook(fromUIButton: ManagePasswordButton)
        passwordButtonLook.addBorderToButton()
        passwordButtonLook.addRadiusToButton()
        
        userNotifier = UserNotifier(parentViewController: self, delegate: self)
        
        fillInUsermail()
    }
    
    func fillInUsermail(){
        let userMail = UserCredentialsManager.getUserName()
        
        if let userMail = userMail{
            let mail = Base64Converter().decodeString(userMail)
            UserEmailLabel.text = mail
        }
        else{ UserEmailLabel.text = "Protected".localized }
    }
    
    func userRemoved(result : NSData?) {
        dispatch_async(dispatch_get_main_queue()) {
            SwiftSpinner.show("Goodbye".localized, animated: false).addTapHandler({
                    SwiftSpinner.hide()
                    let navigator = StoryboardNavigator(storyboardName: "Main")
                    navigator.moveToStoryboard(self)
                }, subtitle: "Your account has been removed".localized)
        }
    }

    private func removeAccount() {
        let deleteSender = DELETESender()
        SwiftSpinner.show("Just a moment".localized)
        deleteSender.deleteUser().execute { (result) -> Void in
            if(deleteSender.wasError()) {
                print(deleteSender.error)
                dispatch_async(dispatch_get_main_queue()){
                    SwiftSpinner.show("Conection error".localized, animated: false).addTapHandler({SwiftSpinner.hide()}, subtitle: UserNotifier.CONNECTION_TIMEOUT_MESSAGE)
                }
                return;
            }
            UserCredentialsManager.removeUserData()
            IBeaconsFinder().stopLookingForAllBeacons()
            self.userRemoved(result)
        }
    }
    
    func userAgreedWithWarningPopup(action: UIAlertAction) {
        removeAccount()
    }
    
    @IBAction func DeleteAccountButtonClicked(sender: AnyObject) {
        userNotifier.warningAgreeActionMessage = WARNING_AGREE_MESSAGE
        userNotifier.cancelActionMessage       = WARNING_CANCEL_MESSAGE
        userNotifier.warningPopup(DELETE_CONFIRMATION_MESSAGE).show()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
