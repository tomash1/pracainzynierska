//
//  GlobalNotificationsHandler.swift
//  Praca Inzynierska
//
//  Created by Tomek Domaracki on 09.11.2015.
//  Copyright © 2015 Tomek Domaracki. All rights reserved.
//

import UIKit

class GlobalNotificationsHandler: NSObject, CLLocationManagerDelegate {

    let locationManager: CLLocationManager
    var currentLocation: CLLocation?
    var beaconRegion: CLBeaconRegion!
    
    override init(){
        locationManager = CLLocationManager()
        currentLocation = nil
        super.init()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.distanceFilter  = kCLDistanceFilterNone
        locationManager.requestWhenInUseAuthorization()
    }
    
    private func didEnterBeaconRegion(){
        print("GLOBAL ENTER")
        
        if CLLocationManager.authorizationStatus() == .AuthorizedAlways {
            locationManager.requestLocation()
        } else {
            print("Location disabled")
        }
    }
    
    private func didExitBeaconRegion(){
        print("GLOBAL EXIT")
//        let notification = UILocalNotification()
//        notification.alertBody = "You lost beacon"
//        notification.soundName = "Default"
//        UIApplication.sharedApplication().presentLocalNotificationNow(notification)
    }
    
    func handleNotification(beaconRegion: CLBeaconRegion, isEnterNotification: Bool?) {
        self.beaconRegion = beaconRegion
        if let enterBeaconRegion = isEnterNotification {
            if enterBeaconRegion {
                didEnterBeaconRegion()
            }
            else {
                didExitBeaconRegion()
            }
        }
    }
    
    @objc func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        print("Failt with error " + error.localizedDescription)
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if locations.count > 0 {
            currentLocation = locations[0]

            let foundNotification = FoundNotification()
            
            foundNotification.uuid = beaconRegion.proximityUUID.UUIDString
            foundNotification.majorId = String(beaconRegion.major!)
            foundNotification.minorId = String(beaconRegion.minor!)
            
            let notification = Notification()
            notification.message = "Found your stuff"
            notification.geoCoordinatesLatitude = currentLocation!.coordinate.latitude//MapKitDefaults.DEFAULT_LATITUDE
            notification.geoCoordinatesLongitude = currentLocation!.coordinate.longitude//MapKitDefaults.DEFAULT_LONGITUDE
            
            foundNotification.notification = notification
            
            POSTSender().addFoundNotification(foundNotification).execute { (result) -> Void in
                
            }
        }
    }
}
