//
//  DELETESender.swift
//  Praca Inzynierska
//
//  Created by Tomasz Domaracki on 05.10.2015.
//  Copyright © 2015 Tomek Domaracki. All rights reserved.
//

import UIKit

class DELETESender: RequestSender {

    let base64Converter = Base64Converter()
    var toDelete = ""
    
    func execute(responseData: (result: NSData?) -> Void) {
        let address = REST_SERVICE_ADDRESS + toDelete
        
        request = NSMutableURLRequest(URL: NSURL(string: address)!)
        request!.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request!.setValue("application/json", forHTTPHeaderField: "Accept")
        request!.HTTPMethod = "DELETE"
        
        addTokenToRequest()
        
        self.send(responseData)
    }
    
    func deleteUser() -> DELETESender {
        if let userEmail = getUserMail() {
            toDelete = "/users/" + userEmail
        }
        return self
    }
    
    func deleteBeacon(beaconToRemove: Beacon) -> DELETESender {
        if let userEmail = getUserMail() {
            if let beaconId = beaconToRemove.id {
                toDelete = "/beacons/" + userEmail + "/" + String(beaconId)
            }
        }
        return self
    }
    
    func removeGlobalNotification(beaconNotification: BeaconExt) -> DELETESender{
        if let beaconId = beaconNotification._Beacon.id {
            toDelete = "/devicenotifications/" + String(beaconId)
        }
        return self
    }
    
    func deleteNotification(notification: Notification) -> DELETESender {
        if let userEmail = getUserMail() {
            if let notificationId = notification.id {
                toDelete = "/notifications/" + userEmail + "/" + String(notificationId)
            }
        }
        return self
    }
    
    func getUserMail() -> String? {
        return UserCredentialsManager.getUserName()
    }
}
