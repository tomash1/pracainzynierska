//
//  NavigationBarLook.swift
//  Praca Inzynierska
//
//  Created by Tomek Domaracki on 05.07.2015.
//  Copyright (c) 2015 Tomek Domaracki. All rights reserved.
//

import UIKit

class NavigationBarLook: NSObject {
    
    let controller : UINavigationController
    
    init(controller : UINavigationController) {
        self.controller = controller
    }
   
    func setLook() {
        
        controller.navigationBar.setBackgroundImage(UIImage(), forBarMetrics: UIBarMetrics.Default)
        controller.navigationBar.shadowImage = UIImage()
        controller.navigationBar.translucent = true
        controller.view.backgroundColor = UIColor(red: 58.0/255.0, green: 58.0/255.0, blue: 58.0/255.0, alpha: 0.23)
        controller.navigationBar.backgroundColor = UIColor(red: 58.0/255.0, green: 58.0/255.0, blue: 58.0/255.0, alpha: 0.23)
        
    }
    
    func addLogoToBarTitle(item : UINavigationItem) {
        
        let logo = UIImage(named: "logoBar.png")
        let imageView = UIImageView(image: logo)
        item.titleView = imageView
    }
    
    func addShadowToBarButton(button : UIBarButtonItem) {
        
        let textShadow = NSShadow()
        textShadow.shadowBlurRadius = 1.0
        textShadow.shadowColor = UIColor(red: 77/255.0, green: 77/255.0, blue: 77/255.0, alpha: 1)
        textShadow.shadowOffset = CGSize(width: 1.0, height: 1.0)
        let shadowAttributeDic :[String:NSShadow] = [NSShadowAttributeName : textShadow]
        button.setTitleTextAttributes(shadowAttributeDic, forState: UIControlState.Normal)
        
    }
}
