//
//  FindStuffTableViewCell.swift
//  Praca Inzynierska
//
//  Created by Tomek Domaracki on 02.11.2015.
//  Copyright © 2015 Tomek Domaracki. All rights reserved.
//

import UIKit

class FindStuffTableViewCell: UITableViewCell {
    
    @IBOutlet var StuffNameLabel: UILabel!
    @IBOutlet var DistanceLabel: UILabel!
    @IBOutlet var LocalNotificationLabel: UILabel!
    @IBOutlet var GlobalNotificationLabel: UILabel!
    
    let ENABLED_MESSAGE  = "Enabled".localized
    let DISABLED_MESSAGE = "Disabled".localized
    let UNKNOWN_MESSAGE  = "Unknown".localized
    
    func withLocalStatus(status: BeaconNotificationStatus) -> FindStuffTableViewCell {
        switch(status){
        case .ON:
            LocalNotificationLabel.text = ENABLED_MESSAGE
        case .OFF:
            LocalNotificationLabel.text = DISABLED_MESSAGE
        default:
            LocalNotificationLabel.text = UNKNOWN_MESSAGE
        }
        return self
    }
    
    func withGlobalStatus(status: BeaconNotificationStatus) -> FindStuffTableViewCell {
        switch(status){
        case .ON:
            GlobalNotificationLabel.text = ENABLED_MESSAGE
        case .OFF:
            GlobalNotificationLabel.text = DISABLED_MESSAGE
        default:
            GlobalNotificationLabel.text = UNKNOWN_MESSAGE
        }
        return self
    }
    
    func withStuffName(name: String) -> FindStuffTableViewCell {
        StuffNameLabel.text = name
        return self
    }
    
    func withDistance(distanceValue: String) -> FindStuffTableViewCell {
        DistanceLabel.text = "Distance " + distanceValue
        return self
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
