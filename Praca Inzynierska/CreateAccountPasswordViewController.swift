//
//  CreateAccountPasswordViewController.swift
//  Praca Inzynierska
//
//  Created by Tomek Domaracki on 30.06.2015.
//  Copyright (c) 2015 Tomek Domaracki. All rights reserved.
//

import UIKit

class CreateAccountPasswordViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet var ContinueButton: UIButton!
    @IBOutlet var PasswordTextField: UITextField!

    var user : User?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let continueButtonLook = ButtonLook(fromUIButton: ContinueButton)
        continueButtonLook.addRadiusToButton()
        let navBarLook = NavigationBarLook(controller: navigationController!)
        navBarLook.addLogoToBarTitle(navigationItem)

        PasswordTextField.delegate = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        view.endEditing(true)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "createSummarySegue"){
            let createUserSummaryViewController = segue.destinationViewController as! CreateAccountSummaryViewController
            
            if let user = user {
                user.passwordHash = PasswordTextField.text
                createUserSummaryViewController.user = user
            }
        }
    }

    override func shouldPerformSegueWithIdentifier(identifier: String, sender: AnyObject?) -> Bool {
        if(identifier == "createSummarySegue"){
            if(PasswordTextField.text == "" || !isValidPassword(PasswordTextField.text)){
                Animator().animateTextFieldError(PasswordTextField)
                return false
            }
        }
        return true
    }
    
    private  func isValidPassword(testStr:String?) -> Bool {
        if let testStr = testStr {
            let passRegEx = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\\s).{4,8}$"
            
            let passTest = NSPredicate(format:"SELF MATCHES %@", passRegEx)
            return passTest.evaluateWithObject(testStr)
        }
        return false
    }
}
