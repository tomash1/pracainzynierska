//
//  Extensions.swift
//  Praca Inzynierska
//
//  Created by Tomek Domaracki on 07.12.2015.
//  Copyright © 2015 Tomek Domaracki. All rights reserved.
//

import UIKit

extension String {
    var localized: String {
        return NSLocalizedString(self, tableName: nil, bundle: NSBundle.mainBundle(), value: "", comment: "")
    }
}
