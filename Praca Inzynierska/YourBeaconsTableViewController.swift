//
//  YourBeaconsTableViewController.swift
//  Praca Inzynierska
//
//  Created by Tomasz Domaracki on 12.10.2015.
//  Copyright © 2015 Tomek Domaracki. All rights reserved.
//

import UIKit
import SwiftyJSON
import SwiftSpinner

protocol YourBeaconsDelegate {
    func newBeaconAdded(beacon: BeaconDeviceExt)
    func beaconRemoved(beacon: BeaconDeviceExt)
}

class YourBeaconsTableViewController: UITableViewController, AddNewBeaconsTableViewControllerDelegate, ConnectBeaconViewControllerDelegate, BLUBeaconManagerDelegate, UserNotifierDelegate {
    var userBeacons : Array<BeaconDeviceExt> = Array()
    
    var selectedIndexPath: NSIndexPath?
    var delegate: YourBeaconsDelegate? = nil
    
    let notUserBeaconsLabel = UILabel()
    var userNotifier: UserNotifier?
    var indexOfBeaconToRemove: NSIndexPath!
    
    var beaconsManager: BLUBeaconManager!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.clipsToBounds  = false
        self.tableView.backgroundView = UIImageView(image: UIImage(named: "app.png")!)
        
        notUserBeaconsLabel.text          = "No beacons".localized
        notUserBeaconsLabel.textColor     = UIColor.whiteColor()
        notUserBeaconsLabel.textAlignment = NSTextAlignment.Center
        notUserBeaconsLabel.frame         = CGRectMake(0, 0, 250, 25)
        notUserBeaconsLabel.center        = CGPointMake(self.view.frame.size.width/2,self.view.frame.size.height/2)
        notUserBeaconsLabel.hidden        = false
        
        self.view.addSubview(notUserBeaconsLabel)
        
        beaconsManager = BLUBeaconManager(delegate: self)
    }
    


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let beaconsCount = userBeacons.count
        if beaconsCount > 0 {
            notUserBeaconsLabel.hidden = true
        }
        else {
            notUserBeaconsLabel.hidden = false
        }
        return beaconsCount
    }


    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier("UserBeaconCell", forIndexPath: indexPath)
        if let userBeaconCell = cell as? UserBeaconTableViewCell {
            let beacon = userBeacons[indexPath.row]
            
            userBeaconCell.withBeaconName( beacon.name )
                          .withBeaconStatus( beacon.status )
            
            cell = userBeaconCell
        }
        return cell
    }
    
    func beaconManager(manager: BLUBeaconManager, didFindBeacon beacon: BLUBeacon) {
        beaconsManager.stopScanning()
        if let indexPath = selectedIndexPath {
            if let configurableBeacon = beacon as? BLUConfigurableBeacon {
                let beaconDeviceExt = userBeacons[indexPath.row]
                SwiftSpinner.hide()
                beaconDeviceExt.BeaconDevice = configurableBeacon
                userBeacons[indexPath.row] = beaconDeviceExt
                performSegueWithIdentifier("ConnectToBeaconSegue", sender: self)
            }
        }
    }
    
    func userCancelledLookingForBeacon() {
        beaconsManager.stopScanning()
        SwiftSpinner.hide()
    }
    
    func userReadedInfoMessage(action: UIAlertAction) {
        SwiftSpinner.show("Looking for beacon".localized).addTapHandler(userCancelledLookingForBeacon, subtitle: "Tap to cancel".localized)
        beaconsManager.startScanningForBeacons()
    }
    
    func connectToBeacon() {
        userNotifier = UserNotifier(parentViewController: self, delegate: self)
        userNotifier!.infoPopup("Beacon connection warning".localized).show()
    }

    func connectedToBeacon(beacon: BeaconDeviceExt?) {
        if let connectedBeacon = beacon {
            updateBeacon(connectedBeacon)
        }
    }
    
    func disconnectFromBeacon(beacon: BeaconDeviceExt){
        beacon.BeaconDevice.disconnect()
        switch(beacon.status){
        case BeaconAvailability.Connected:
            beacon.status = BeaconAvailability.Disconnected
            break
        case BeaconAvailability.NotConfigured:
            beacon.status = BeaconAvailability.DisconnectedNC
            break
        default:
            beacon.status = BeaconAvailability.Unknown
        }
        tableView.reloadData()
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        self.selectedIndexPath = indexPath
        let beacon = userBeacons[self.selectedIndexPath!.row]
        
        if beacon.status == BeaconAvailability.Disconnected || beacon.status == BeaconAvailability.DisconnectedNC {
            connectToBeacon()
        }
        else {
            disconnectFromBeacon(beacon)
        }
    }
    
    func addNewBeaconsDidConnectToBeacon(beacon: BeaconDeviceExt) {
        if let yourDelegate = delegate {
            yourDelegate.newBeaconAdded(beacon)
        }
        userBeacons.append(beacon)
        tableView.reloadData()
    }
    
    func beaconIsDisconnected(disconnectedBeacon: BeaconDeviceExt) {
        updateBeacon(disconnectedBeacon)
    }
    
    func updateBeacon(beacon: BeaconDeviceExt) {
        let index = (userBeacons as NSArray).indexOfObject(beacon)
        userBeacons[index] = beacon
        tableView.reloadData()
    }

    
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    
    func beaconRemoved(result: NSData?) {
        if let _ = result {
            dispatch_async(dispatch_get_main_queue()) {
                let beacon = self.userBeacons[self.indexOfBeaconToRemove.row]
                self.removeBeaconRegion(beacon)
                if let yourDelegate = self.delegate {
                    yourDelegate.beaconRemoved(beacon)
                }
                self.userBeacons.removeAtIndex(self.indexOfBeaconToRemove.row)
                self.tableView.deleteRowsAtIndexPaths([self.indexOfBeaconToRemove], withRowAnimation: .Fade)
            }
        }
    }
    
    func removeBeaconRegion(beacon: BeaconDeviceExt){
        let beaconExt = BeaconExt(beacon: beacon)
        if let region = beaconExt.getBeaconRegion() {
            BeaconRegionsFileManager().removeRegion(region)
        }
    }
    
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            var beacon: BeaconDeviceExt? = nil
            if userBeacons.count >= indexPath.row {
                beacon = userBeacons[indexPath.row]
            }
            if let beaconDeviceExt = beacon {
                indexOfBeaconToRemove = indexPath
                if beaconDeviceExt.isConnected() { disconnectFromBeacon(beaconDeviceExt) }
                
                DELETESender().deleteBeacon(beaconDeviceExt as Beacon).execute(beaconRemoved)
            }
        }
    }
    

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */
    
    
    // MARK: - Navigation

    @IBAction func prepareForUnwindToYourBeacons(segue : UIStoryboardSegue) {}
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "ConnectToBeaconSegue" {
            if let destinationViewController = segue.destinationViewController as? ConnectBeaconViewController {
                destinationViewController.selectedUserBeacon = userBeacons[self.selectedIndexPath!.row]
                destinationViewController.beaconManager = beaconsManager
                destinationViewController.unwindToViewController = "unwindToYourBeacons"
                destinationViewController.delegate = self
            }
        }
    }


}
