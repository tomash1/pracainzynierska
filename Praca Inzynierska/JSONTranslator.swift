//
//  JSONTranslator.swift
//  Praca Inzynierska
//
//  Created by Tomek Domaracki on 02.11.2015.
//  Copyright © 2015 Tomek Domaracki. All rights reserved.
//

import UIKit
import SwiftyJSON

class JSONTranslator: NSObject {

    static func convertJsonToUserNotificationsArray(notificationsArrayJSON: JSON) -> Array<Notification>{
        var notificationsArray: Array<Notification> = Array()
        for notificationJSON in notificationsArrayJSON.arrayValue {
            if let notificationRaw = notificationJSON.stringValue.dataUsingEncoding(NSUTF8StringEncoding) {
                let notification = Notification.fromJSON(notificationRaw)!
                notificationsArray.append(notification)
            }
        }
        return notificationsArray
    }
    
    static func convertJsonToNotificationsArray(notificationsArrayJSON: JSON) -> Array<GlobalNotification>{
        var notificationsArray: Array<GlobalNotification> = Array()
        for notificationJSON in notificationsArrayJSON.arrayValue {
            if let notificationRaw = notificationJSON.stringValue.dataUsingEncoding(NSUTF8StringEncoding) {
                let notification = GlobalNotification.fromJSON(notificationRaw)!
                notificationsArray.append(notification)
            }
        }
        return notificationsArray
    }
    
    static func convertJsonToBeaconsArray(beaconsArrayJSON: JSON) -> Array<Beacon>{
        var beaconsArray: Array<Beacon> = Array()
        for beaconJSON in beaconsArrayJSON.arrayValue {
            if let beaconRaw = beaconJSON.stringValue.dataUsingEncoding(NSUTF8StringEncoding) {
                let beacon = Beacon.fromJSON(beaconRaw)!
                beaconsArray.append(beacon)
            }
        }
        return beaconsArray
    }
    
    static func convertJsonToBeaconsExtArray(beaconsArrayJSON: JSON) -> Array<BeaconExt>{
        let beaconsArray: Array<Beacon> = convertJsonToBeaconsArray(beaconsArrayJSON)
        var beaconsExtArray: Array<BeaconExt> = Array()
        for beacon in beaconsArray {
            beaconsExtArray.append(BeaconExt(beacon: beacon))
        }
        return beaconsExtArray
    }
}
