//
//  SignInViewController.swift
//  Praca Inzynierska
//
//  Created by Tomek Domaracki on 28.06.2015.
//  Copyright (c) 2015 Tomek Domaracki. All rights reserved.
//

import UIKit
import SwiftSpinner
import SwiftyJSON

enum SignInDataType{
    case Password
    case Email
    case All
}

class SignInViewController: UIViewController, UITextFieldDelegate, UIGestureRecognizerDelegate, AuthenticatorDelegate {

    var viewCenterX : CGFloat!
    
    @IBOutlet var SignInButton: UIButton!
    @IBOutlet var BackButton: UIButton!
    @IBOutlet weak var WrongCredentialsLabel: UILabel!
    
    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var passwordTextField: UITextField!

    let authenticator = Authenticator()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let signInButton = ButtonLook(fromUIButton: SignInButton)
        signInButton.addRadiusToButton()
        
        let leftEdgeGestureRecognizer = UIScreenEdgePanGestureRecognizer(target: self, action: Selector("handleLeftEdgeGesture:"))
        leftEdgeGestureRecognizer.edges = UIRectEdge.Left
        leftEdgeGestureRecognizer.delegate = self
        
        //view.addGestureRecognizer(leftEdgeGestureRecognizer)
        
        viewCenterX = self.view.bounds.size.width / 2.0

        emailTextField.delegate = self
        passwordTextField.delegate = self
        authenticator.delegate = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Keyboard actions
    
    func handleLeftEdgeGesture(gesture : UIScreenEdgePanGestureRecognizer){
        let view = self.view.hitTest(gesture.locationInView(gesture.view), withEvent: nil)
        if(UIGestureRecognizerState.Began == gesture.state || UIGestureRecognizerState.Changed == gesture.state) {
                let translation = gesture.translationInView(gesture.view)
                if(translation.x > viewCenterX){
                    unwindToMainViewController()
                }
                view!.center = CGPointMake(viewCenterX + translation.x, view!.center.y)
        } else {
            view!.center = CGPoint(x: viewCenterX, y: view!.center.y)
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if emailTextField.isFirstResponder() {
            passwordTextField.becomeFirstResponder()
        }
        else {
            passwordTextField.resignFirstResponder()
        }
        return false;
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        view.endEditing(true)
    }
    
    // MARK: - Buttons actions
    
    @IBAction func SignInButtonClicked(sender: AnyObject) {
        let mail = emailTextField.text!
        let pass = passwordTextField.text!
        
        if let emptyData = authDataIsEmpty(mail, pass: pass){
            signInDataIsEmpty(emptyData)
        }
        else {
            SwiftSpinner.show(UserNotifier.AUTH_MESSAGE)
            view.endEditing(true)
            
            authenticator.authenticate(mail, password: pass)
        }
    }
    
    private func authDataIsEmpty(mail: String, pass: String) -> SignInDataType? {
        if(mail.isEmpty || pass.isEmpty){
            if(mail != ""){
                return .Password
            }
            if(pass != ""){
                return .Email
            }
            return .All
        }
        return nil
    }
    
    
    func signInDataIsEmpty(onType : SignInDataType) {
        let animator = Animator()
        if(onType == .Email || onType == .All){
            animator.animateTextFieldError(emailTextField)
        }
        if(onType == .Password || onType == .All){
            animator.animateTextFieldError(passwordTextField)
        }
    }
    
    // MARK: - Authenticator delegate methods
    
    func authenticatorDidAuthenticateUser(){
        SwiftSpinner.hide()
        moveToMainMenuStoryboard()
    }
    
    func authenticatorFailToAuthenticateUser(message: String?){
        dispatch_async(dispatch_get_main_queue()) {
            SwiftSpinner.show("Wrong credentials", animated: false).addTapHandler({
                SwiftSpinner.hide()
                }, subtitle: UserNotifier.WRONG_CREDENTIALS_MESSAGE)
        }
    }
    
    func authenticatorTimeout(message: String){
        dispatch_async(dispatch_get_main_queue()) {
            SwiftSpinner.show("Connection error", animated: false).addTapHandler({
                SwiftSpinner.hide()
                }, subtitle: UserNotifier.CONNECTION_TIMEOUT_MESSAGE)
        }
    }
    
    // MARK: - Navigation
    
    func moveToMainMenuStoryboard(){
        let navigator = StoryboardNavigator(storyboardName: "LogedUserStory")
        navigator.moveToStoryboard(self)
    }
    
    func unwindToMainViewController(){}
    
//    @IBAction func unwindToMainViewController(sender: UIStoryboardSegue) {
//        self.dismissViewControllerAnimated(true, completion: nil)
//    }
    
//    let animator = Animator()
//    
//    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
//        let toViewController = segue.destinationViewController as! UIViewController
//        
//        toViewController.transitioningDelegate = animator   
//    }
}
