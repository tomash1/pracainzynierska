//
//  BeaconExt.swift
//  Praca Inzynierska
//
//  Created by Tomek Domaracki on 02.11.2015.
//  Copyright © 2015 Tomek Domaracki. All rights reserved.
//

import UIKit

enum BeaconNotificationStatus{
    case ON
    case OFF
    case UNKNOWN
}

class BeaconExt: NSObject {

    let _Beacon: Beacon!
    
    var proximity: CLProximity!
    var observing: Bool!
    var distance: CLLocationAccuracy!
    var rssi: Int!
    
    init(beacon: Beacon) {
        _Beacon = beacon
        super.init()
        initValues()
    }

    init(beaconExt: BeaconExt) {
        _Beacon = beaconExt._Beacon
        super.init()
        initValues()
    }
    
    init(beaconDeviceExt: BeaconDeviceExt) {
        let beacon = Beacon()
        beacon.majorId  = beaconDeviceExt.majorId
        beacon.minorId  = beaconDeviceExt.minorId
        beacon.uuid     = beaconDeviceExt.uuid
        beacon.name     = beaconDeviceExt.name
        beacon.deviceId = beaconDeviceExt.deviceId
        beacon.id       = beaconDeviceExt.id
        beacon.userId   = beaconDeviceExt.userId
        _Beacon = beacon
        
        super.init()
        initValues()
    }
    
    private func initValues(){
        proximity = CLProximity.Unknown
        observing = false
        distance  = CLLocationAccuracy(0.0)
        rssi = 0
    }
    
    func getProximityUUID() -> NSUUID? {
        if let beaconUUID = _Beacon.uuid{
            if let UUID = NSUUID(UUIDString: beaconUUID) {
                return UUID
            }
        }
        return nil
    }
    
    func getUUID() -> String {
        if let uuid = _Beacon.uuid {
            return uuid
        }
        return "UUID unavailable".localized
    }
    
    func getMajorId() -> String {
        if let major = _Beacon.majorId {
            return major
        }
        return "Major unavailable".localized
    }
    
    func getMinorId() -> String {
        if let minor = _Beacon.minorId {
            return minor
        }
        return "Minor unavailable".localized
    }
    
    func getMajorId() -> Int? {
        if let beaconMajor = _Beacon.majorId {
            if let major = Int(beaconMajor) {
                return major
            }
        }
        return nil
    }
    
    func getMinorId() -> Int? {
        if let beaconMinor = _Beacon.minorId {
            if let minor = Int(beaconMinor) {
                return minor
            }
        }
        return nil
    }
    
    func getName() -> String {
        return _Beacon.name!
    }
    
    func isLocalNotificationEnabled() -> BeaconNotificationStatus {
        if observing == true {
            return BeaconNotificationStatus.ON
        } else {
            return BeaconNotificationStatus.OFF
        }
    }
    
    func getStringRSSI() -> String {
        return String(rssi)
    }
    
    func getStringProximity() -> String {
        return stringForBeaconProximity(proximity)
    }
    
    func getStringDistance() -> String {
        return String(format: "%.2f", distance) + "m"
    }
    
    func isEqualToClBeacon(clBeacon: CLBeacon) -> Bool {
        let strMaj  = String(clBeacon.major)
        let strMin  = String(clBeacon.minor)
        let strUUID = clBeacon.proximityUUID.UUIDString.uppercaseString
        
        let thisUUID  = _Beacon.uuid!.uppercaseString
        let thisMajor = _Beacon.majorId!
        let thisMinor = _Beacon.minorId!
        
        if strUUID == thisUUID &&
            strMaj == thisMajor &&
            strMin == thisMinor {
                return true
        }
        return false
    }
    
    func getBeaconRegion() -> CLBeaconRegion? {
        if let uuid = NSUUID(UUIDString: _Beacon.uuid!) {
            if let minorId = Int(_Beacon.minorId!) {
                if let majorId = Int(_Beacon.majorId!) {
                    let beaconRegion = CLBeaconRegion(proximityUUID: uuid, major: UInt16(majorId), minor: UInt16(minorId), identifier: getName())
                    return beaconRegion
                }
            }
        }
        return nil
    }
    
    private func stringForBeaconProximity(proximity: CLProximity) -> String {
        switch(proximity) {
        case .Far:
            return "Far".localized
        case .Near:
            return "Near".localized
        case .Immediate:
            return "Immediate".localized
        case .Unknown:
            return "Unknown".localized
        }
    }
}
