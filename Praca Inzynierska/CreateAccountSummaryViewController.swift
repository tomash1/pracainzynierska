//
//  CreateAccountSummaryViewController.swift
//  Praca Inzynierska
//
//  Created by Tomek Domaracki on 30.06.2015.
//  Copyright (c) 2015 Tomek Domaracki. All rights reserved.
//

import UIKit
import SwiftSpinner

class CreateAccountSummaryViewController: UIViewController, AuthenticatorDelegate {
    
    @IBOutlet var CreateAccountButton: UIButton!
    @IBOutlet weak var EmailTextField: UILabel!
    @IBOutlet weak var PasswordTextField: UILabel!
    @IBOutlet var ShowPasswordButton: UIButton!

    var user : User?
    var userPassHidden = ""
    
    let authenticator    = Authenticator()
    let createUserSender = POSTSender()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let createAccountButtonLook = ButtonLook(fromUIButton: CreateAccountButton)
        createAccountButtonLook.addRadiusToButton()
        
        let showPasswordButtonLook = ButtonLook(fromUIButton: ShowPasswordButton)
        showPasswordButtonLook.addBorderToButton()
        showPasswordButtonLook.addRadiusToButton()
        
        if let user = user {
            prepareUserPass()
            
            EmailTextField.text    = user.email
            PasswordTextField.text = userPassHidden
            authenticator.delegate = self
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation
    
    func moveToLogedUserStoryBoard(){
        let navigator = StoryboardNavigator(storyboardName: "LogedUserStory")
        navigator.moveToStoryboard(self)
    }

    // MARK: - Buttons actions
    
    @IBAction func ShowPasswordButtonTouchedDown(sender: AnyObject) {
        if let user = user {
            PasswordTextField.text = user.passwordHash
        }
    }
    
    @IBAction func ShowPasswordButtonTouchedUpInside(sender: AnyObject) {
        PasswordTextField.text = userPassHidden
    }
    
    
    @IBAction func CreateAccountButtonClicked(sender: AnyObject) {
        SwiftSpinner.show("Creating your account")
        
        if let user = user {
            createUserSender.addUser(user).execute { (result) -> Void in
                if self.createUserSender.wasError(){
                    print(self.createUserSender.error)

                    self.showTimeoutSpinner()
                    return;
                }
                
                self.authenticator.authenticate(self.user!.email!, password: self.user!.passwordHash!)
            }
        }
    }
    
    // MARK: - Authenticator delegate methods
    
    func authenticatorDidAuthenticateUser(){
        SwiftSpinner.hide()
        self.moveToLogedUserStoryBoard()
    }
    
    func authenticatorFailToAuthenticateUser(message: String?){
        var messageToPresent = ""
        if let message = message {
            messageToPresent = message
        }
        print("User failed to authenticate " + messageToPresent)
    }
    
    func authenticatorTimeout(message: String){
        showTimeoutSpinner()
    }
    
    // MARK: - Private methods
    
    private func  showTimeoutSpinner(){
        dispatch_async(dispatch_get_main_queue()) {
            SwiftSpinner.show("Conection error".localized, animated: false).addTapHandler({SwiftSpinner.hide()}, subtitle: UserNotifier.CONNECTION_TIMEOUT_MESSAGE)
        }
    }
    
    private func prepareUserPass(){
        if let passHash = user!.passwordHash {
            for _ in 1...passHash.characters.count {
                userPassHidden += "\u{2022}"
            }
        }
    }
}
