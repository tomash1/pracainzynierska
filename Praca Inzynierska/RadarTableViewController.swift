//
//  RadarTableViewController.swift
//  Praca Inzynierska
//
//  Created by Tomek Domaracki on 09.11.2015.
//  Copyright © 2015 Tomek Domaracki. All rights reserved.
//

import UIKit

class RadarTableViewController: UITableViewController {
    var userBeacons : Array<BeaconDeviceExt> = Array()    
    let notUserBeaconsLabel = UILabel()
    var selectedIndexPath: NSIndexPath?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.clipsToBounds  = false
        self.tableView.backgroundView = UIImageView(image: UIImage(named: "app.png")!)
        
        notUserBeaconsLabel.text          = "No beacons".localized
        notUserBeaconsLabel.textColor     = UIColor.whiteColor()
        notUserBeaconsLabel.textAlignment = NSTextAlignment.Center
        notUserBeaconsLabel.frame         = CGRectMake(0, 0, 250, 25)
        notUserBeaconsLabel.center        = CGPointMake(self.view.frame.size.width/2,self.view.frame.size.height/2)
        notUserBeaconsLabel.hidden        = false
        
        self.view.addSubview(notUserBeaconsLabel)

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let beaconsCount = userBeacons.count
        if beaconsCount > 0 {
            notUserBeaconsLabel.hidden = true
        }
        else {
            notUserBeaconsLabel.hidden = false
        }
        return beaconsCount
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("RadarBeaconCell", forIndexPath: indexPath)
        let beacon = userBeacons[indexPath.row]
        cell.textLabel?.text = beacon.name!
        cell.detailTextLabel?.text = String(beacon.signalStrength)

        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        selectedIndexPath = indexPath
        performSegueWithIdentifier("RadarBeaconSelectedSegue", sender: self)
    }

    
    // MARK: - Navigation

    @IBAction func prepareForUnwindToRadarTableViewController(segue: UIStoryboardSegue){}
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "RadarBeaconSelectedSegue" {
            if let beaconRadarViewController = segue.destinationViewController as? BeaconRadarViewController {
                if let indexPath = selectedIndexPath {
                    beaconRadarViewController.selectedBeacon = BeaconExt(beaconDeviceExt: userBeacons[indexPath.row])
                }
            }
        }
    }

}
