//
//  Authenticator.swift
//  Praca Inzynierska
//
//  Created by Tomek Domaracki on 24.09.2015.
//  Copyright (c) 2015 Tomek Domaracki. All rights reserved.
//

import UIKit
import SwiftyJSON

protocol AuthenticatorDelegate {
    func authenticatorDidAuthenticateUser()
    func authenticatorFailToAuthenticateUser(message: String?)
    func authenticatorTimeout(message: String)
}

class Authenticator: RequestSender {
    
    let AUTH_ADDRESS = "/authentication"
    
    var email : String!
    var pass  : String!
    var loginForToken : String!

    var delegate: AuthenticatorDelegate?

    let base64Converter = Base64Converter()
    
    func authenticate(mail : String, password : String) {
        setEmailValue(mail)
        setPasswordValue(password)
        
        authenticateOnServer { (result) -> Void in
            if (self.wasError()){
                print(self.error)
                if let delegate = self.delegate {
                    delegate.authenticatorTimeout(self.error)
                    return
                }
            }
            self.assignUserValues(result)
        }
    }
    
    private func setEmailValue(email : String){
        self.email = base64Converter.encodeString(email)
    }
    
    private func setPasswordValue(pass : String){
        self.pass = base64Converter.encodeString(pass)
    }
    
    private func prepareAuthData(){
        loginForToken = email + "::??::" + pass
    }

    private func authenticateOnServer(responseData: (result: NSData?) -> Void) {
        let address = REST_SERVICE_ADDRESS + AUTH_ADDRESS
        prepareAuthData()
        
        request = NSMutableURLRequest(URL: NSURL(string: address)!)
        request!.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request!.setValue("application/json", forHTTPHeaderField: "Accept")
        request!.setValue(loginForToken, forHTTPHeaderField: "loginForToken")
        
        request!.HTTPMethod = "GET"
        
        self.send(responseData)
    }
    
    private func userResponse(result: NSData?) {
        if let delegate = delegate {
            if let userData = result {
                if let user = User.fromJSON(userData) {
                    if let userId = user.id{
                        UserCredentialsManager.setUserId(userId)
                        delegate.authenticatorDidAuthenticateUser()
                        return
                    }
                }
            }
        delegate.authenticatorFailToAuthenticateUser(nil)
        }
    }
    
    private func assignUserValues(data : NSData?){
        var success = false
        if let data = data {
            if let token = NSString(data: data, encoding: NSUTF8StringEncoding) {
                UserCredentialsManager.setUserName(email)
                UserCredentialsManager.setUserNSToken(token)
                
                GETSender().user().execute(userResponse)
                success = true
            }
        }
        if !success {
            if let delegate = delegate {
                delegate.authenticatorFailToAuthenticateUser(nil)
            }
        }
    }
}
