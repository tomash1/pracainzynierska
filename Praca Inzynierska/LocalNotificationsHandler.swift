//
//  LocalNotificationsHandler.swift
//  Praca Inzynierska
//
//  Created by Tomek Domaracki on 09.11.2015.
//  Copyright © 2015 Tomek Domaracki. All rights reserved.
//

import UIKit

class LocalNotificationsHandler {

    private func didEnterBeaconRegion(){
        let notification = UILocalNotification()
        notification.alertBody =  "Local Notification Enter".localized
        notification.soundName = "Default"
        UIApplication.sharedApplication().presentLocalNotificationNow(notification)
    }
    
    private func didExitBeaconRegion(){
        let notification = UILocalNotification()
        notification.alertBody = "Local Notification Exit".localized
        notification.soundName = "Default"
        UIApplication.sharedApplication().presentLocalNotificationNow(notification)
    }
    
    func handleNotification(isEnterNotification: Bool?) {
        if let enterBeaconRegion = isEnterNotification {
            if enterBeaconRegion {
                didEnterBeaconRegion()
            }
            else {
                didExitBeaconRegion()
            }
        }
    }
    
}
