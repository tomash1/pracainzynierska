//
//  NotificationMapPin.swift
//  Praca Inzynierska
//
//  Created by Tomek Domaracki on 24.11.2015.
//  Copyright © 2015 Tomek Domaracki. All rights reserved.
//

import UIKit
import MapKit
import Contacts

class NotificationMapPin: NSObject, MKAnnotation {
    
    var coordinate: CLLocationCoordinate2D
    var title: String?
    var subtitle: String?
    
    init(notification: Notification) {
        var tmpCoordinate: CLLocationCoordinate2D?
        if let latitude = notification.geoCoordinatesLatitude {
            if let longitude = notification.geoCoordinatesLongitude {
                tmpCoordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
                title    = notification.message
                subtitle = notification.timestamp
            }
        }
        if tmpCoordinate == nil {
            tmpCoordinate = CLLocationCoordinate2D(latitude: MapKitDefaults.DEFAULT_LATITUDE, longitude: MapKitDefaults.DEFAULT_LONGITUDE)
        }
        coordinate = tmpCoordinate!
        super.init()
    }
    
    func mapItem() -> MKMapItem {
        let placemark = MKPlacemark(coordinate: coordinate, addressDictionary: nil)
        
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = title
        
        return mapItem
    }

}
