//
//  GETSender.swift
//  Praca Inzynierska
//
//  Created by Tomasz Domaracki on 01.10.2015.
//  Copyright © 2015 Tomek Domaracki. All rights reserved.
//

import UIKit

class GETSender: RequestSender {

    var toGetAddress = ""
    let base64Converter = Base64Converter()
    
    func execute(responseData: (result: NSData?) -> Void) {
        let address = REST_SERVICE_ADDRESS + toGetAddress
        
        request = NSMutableURLRequest(URL: NSURL(string: address)!)
        request!.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request!.setValue("application/json", forHTTPHeaderField: "Accept")
        request!.HTTPMethod = "GET"
        
        addTokenToRequest()
        
        self.send(responseData)
    }
    
    func user() -> GETSender {
        if let userEmail = getUserMail() {
            toGetAddress = "/users/" + userEmail
        }
        return self
    }
    
    func beacons() -> GETSender {
        if let userEmail = getUserMail() {
            toGetAddress = "/beacons/" + userEmail
        }
        return self
    }
    
    func beaconsCredentials() -> GETSender {
        if let userEmail = getUserMail() {
            toGetAddress = "/beaconcredentials/" + userEmail
        }
        return self
    }
    
    func globalNotifications() -> GETSender {
        toGetAddress = "/devicenotifications"
        return self
    }
    
    func userNotifications() -> GETSender {
        if let userEmail = getUserMail() {
            toGetAddress = "/notifications/" + userEmail
        }
        return self
    }
    
    private func getUserMail() -> String? {
        return UserCredentialsManager.getUserName()
    }
}
