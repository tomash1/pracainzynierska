//
//  RequestSender.swift
//  Praca Inzynierska
//
//  Created by Tomek Domaracki on 24.09.2015.
//  Copyright (c) 2015 Tomek Domaracki. All rights reserved.
//

import UIKit

class RequestSender: NSObject {
    //let REST_SERVICE_ADDRESS = "http://5.196.227.113:8800/FindStuffService/restService"
    //let REST_SERVICE_ADDRESS = "http://192.168.0.70:8080/FindStuffService/restService"
    let REST_SERVICE_ADDRESS = "http://localhost:8080/FindStuffService/restService"
    
    var restData : NSData? = nil
    var error : String = ""
    var request : NSMutableURLRequest?
    
    let restDictionary : NSDictionary!
    
    override init() {
        let restDictionaryFilePath = NSBundle.mainBundle().pathForResource("RESTCommunication", ofType: "plist")!
        restDictionary = NSDictionary(contentsOfFile: restDictionaryFilePath)
    }
    
    func wasError() -> Bool{
        if error != "" || restData == nil {
            return true
        }
        return false
    }
    
    func addTokenToRequest() {
        var username: String!
        var token: String!

        username = UserCredentialsManager.getUserName()
        token    = UserCredentialsManager.getToken()

        if token != nil && username != nil {
            let auth = username! + ":" + token!
            request!.setValue(auth, forHTTPHeaderField: "authorization")
        }
    }
    
    func send(dataReceived: (result: NSData?) -> Void){
        if(request == nil) {
            dataReceived(result: nil)
        }
        error = ""
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request!){
            data, response, error in
            let httpResponse = response as? NSHTTPURLResponse
            self.restData = nil
            
            if(httpResponse != nil){
                let status = httpResponse!.statusCode
                if error == nil{
                    if status == 200 {
                        self.restData = data
                    } else if status == 401 {
                        print("No authorization - 401")
                    } else {
                        print("Response code: " + String(status))
                    }
                }
                else {
                    if error != nil {
                        self.error = String(error!.localizedDescription)
                    }
                    else{
                        self.error = String(status)
                    }
                    print("Connection error " + self.error)
                }
            }
            dataReceived(result: self.restData)
        }
        task.resume()
    }
}
