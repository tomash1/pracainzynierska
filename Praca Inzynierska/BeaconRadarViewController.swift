//
//  BeaconRadarViewController.swift
//  Praca Inzynierska
//
//  Created by Tomek Domaracki on 09.11.2015.
//  Copyright © 2015 Tomek Domaracki. All rights reserved.
//

import UIKit

class BeaconRadarViewController: UIViewController, CLLocationManagerDelegate, IBeaconsFinderDelegate {

    @IBOutlet var BeaconNameLabel :  UILabel!
    @IBOutlet var MajorValueLabel :  UILabel!
    @IBOutlet var MinorValueLabel :  UILabel!
    @IBOutlet var UUIDValueLabel  :  UILabel!
    @IBOutlet var ProximityLabel  :  UILabel!
    @IBOutlet var RSSILabel       :  UILabel!
    @IBOutlet var DistanceLabel   :  UILabel!
    @IBOutlet var DoneButton      :  UIButton!
    
    var selectedBeacon: BeaconExt?
    
    var finder = IBeaconsFinder()
    var removeRangedBeacon = false
    
    private func initValues() {
        if let beacon = selectedBeacon {
            BeaconNameLabel.text = beacon.getName()
            MajorValueLabel.text = beacon.getMajorId()
            MinorValueLabel.text = beacon.getMinorId()
            UUIDValueLabel.text  = beacon.getUUID()
            ProximityLabel.text  = beacon.getStringProximity()
            RSSILabel.text       = beacon.getStringRSSI()
            
            DistanceLabel.text   = "Looking".localized
        }
    }
    
    private func reloadValues() {
        if let beacon = selectedBeacon {
            ProximityLabel.text = beacon.getStringProximity()
            DistanceLabel.text  = beacon.getStringDistance()
        }
    }
    
    override func viewDidLoad() {
        finder.delegate = self
        if let beacon = selectedBeacon {
            if let beaconRegion = beacon.getBeaconRegion() {
                if !finder.lookingfForRegion(beaconRegion) {
                    removeRangedBeacon = true
                }
                finder.startRangingBeaconsInRegion(beaconRegion)
            }
        }
        
        let buttonLook = ButtonLook(fromUIButton: DoneButton)
        buttonLook.addBorderToButton()
        buttonLook.addRadiusToButton()
        
        initValues()
        super.viewDidLoad()
    }
    
    override func viewWillDisappear(animated: Bool) {
        if removeRangedBeacon {
            if let beacon = selectedBeacon {
                if let beaconRegion = beacon.getBeaconRegion() {
                    finder.stopRangingBeaconsInRegion(beaconRegion)
                }
            }
        }
        super.viewWillDisappear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func updateValuesWithClBeacons(clBeacon: CLBeacon) {
        let accuracy = clBeacon.accuracy
        if accuracy != -1.0 { selectedBeacon!.distance = accuracy }
        
        selectedBeacon!.rssi      = clBeacon.rssi
        selectedBeacon!.proximity = clBeacon.proximity
        reloadValues()
    }

    func finderDidRangeBeacons(beacons: [CLBeacon]){
        for beacon in beacons {
            if let beaconExt = selectedBeacon {
                if beaconExt.isEqualToClBeacon(beacon){
                    updateValuesWithClBeacons(beacon)
                }
            }
        }
    }
}
