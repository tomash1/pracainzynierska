//
//  NotificationMapViewController.swift
//  Praca Inzynierska
//
//  Created by Tomek Domaracki on 23.11.2015.
//  Copyright © 2015 Tomek Domaracki. All rights reserved.
//

import UIKit
import MapKit

class MapKitDefaults: NSObject {
    static let DEFAULT_LATITUDE  = 50.2649
    static let DEFAULT_LONGITUDE = 19.0238
    static let METERS_PER_MILE   = 1609.344
}

class NotificationMapViewController: UIViewController, MKMapViewDelegate {
    @IBOutlet var MapView: MKMapView!
    
    var notification: Notification?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        MapView.delegate = self
        
        zoomNotificationDataOnMap()
        assignNotificationToMap()
    }
    
    func assignNotificationToMap(){
        if let notification = notification {
            let mapPin = NotificationMapPin(notification: notification)
            
            MapView.addAnnotation(mapPin)
        }
    }
    
    func zoomNotificationDataOnMap(){
        if let beaconNotification = notification {
            if let latitude = beaconNotification.geoCoordinatesLatitude {
                if let longitude = beaconNotification.geoCoordinatesLongitude {
                    var location = CLLocationCoordinate2D()
                    location.latitude  = latitude
                    location.longitude = longitude
                    
                    let region = MKCoordinateRegionMakeWithDistance( location,
                                                                     0.5 * MapKitDefaults.METERS_PER_MILE,
                                                                     0.5 * MapKitDefaults.METERS_PER_MILE )
                    MapView.setRegion(region, animated: true)
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        if let annotation = annotation as? NotificationMapPin {
            let identifier = "pin"
            var view: MKPinAnnotationView
            if let dequeuedView = mapView.dequeueReusableAnnotationViewWithIdentifier(identifier)
                as? MKPinAnnotationView {
                    dequeuedView.annotation = annotation
                    view = dequeuedView
            } else {
                view = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                view.canShowCallout = true
                view.calloutOffset = CGPoint(x: -5, y: 5)
                view.rightCalloutAccessoryView = UIButton(type: .DetailDisclosure) as UIView
            }
            return view
        }
        return nil
    }
    
    func mapView(mapView: MKMapView, didAddAnnotationViews views: [MKAnnotationView]) {
        if let lastAnnotation = MapView.annotations.last{
            MapView.selectAnnotation(lastAnnotation, animated: true)
        }
    }
    
    func mapView(mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        let location = view.annotation as! NotificationMapPin
        let launchOptions = [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving]
        location.mapItem().openInMapsWithLaunchOptions(launchOptions)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
