//
//  ManageBeaconsViewController.swift
//  Praca Inzynierska
//
//  Created by Tomasz Domaracki on 09.10.2015.
//  Copyright © 2015 Tomek Domaracki. All rights reserved.
//

import UIKit

class ManageBeaconsViewController: UITabBarController {
    
    var userBeacons: Array<BeaconDeviceExt> = Array()
    var yourBeaconDelegate: YourBeaconsDelegate? = nil
    
    var yourBeaconsTableViewController: YourBeaconsTableViewController?
    var addNewBeaconsTableViewController: AddNewBeaconsTableViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBar.barTintColor = UIColor.clearColor()
        self.tabBar.tintColor = UIColor.whiteColor()
        if let tabBarViewControllers = viewControllers {
            for viewController in tabBarViewControllers {
                if let controller = viewController as? YourBeaconsTableViewController {
                    yourBeaconsTableViewController = controller
                }
                if let controller = viewController as? AddNewBeaconsTableViewController {
                    addNewBeaconsTableViewController = controller
                }
            }
            if yourBeaconsTableViewController != nil && addNewBeaconsTableViewController != nil {
                addNewBeaconsTableViewController!.delegate = yourBeaconsTableViewController!
                yourBeaconsTableViewController!.userBeacons = userBeacons
                if let yourDelegate = yourBeaconDelegate {
                    yourBeaconsTableViewController!.delegate = yourDelegate
                }
            }
        }
        // Do any additional setup after loading the view.
    }
    
    override func viewWillDisappear(animated: Bool) {
        if let yourBeaconsController = yourBeaconsTableViewController {
            for beacon in yourBeaconsController.userBeacons {
                if beacon.isConnected() {
                    yourBeaconsController.disconnectFromBeacon(beacon)
                }
            }
        }
        super.viewWillDisappear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
