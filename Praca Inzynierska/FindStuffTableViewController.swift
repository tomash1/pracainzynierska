//
//  FindStuffTableViewController.swift
//  Praca Inzynierska
//
//  Created by Tomek Domaracki on 02.11.2015.
//  Copyright © 2015 Tomek Domaracki. All rights reserved.
//

import UIKit
import SwiftyJSON
import SwiftSpinner
import CoreLocation

class FindStuffTableViewController: UITableViewController, IBeaconsFinderDelegate, UserNotifierDelegate {

    var userBeacons: Array<BeaconExt> = Array()
    
    var beaconsFinder: IBeaconsFinder!
    let notUserBeaconsLabel = UILabel()
    
    var userNotifier: UserNotifier!
    var selectedBeacon: BeaconExt?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        beaconsFinder = IBeaconsFinder()
        beaconsFinder.delegate = self
        
        self.tableView.clipsToBounds  = false
        self.tableView.backgroundView = UIImageView(image: UIImage(named: "app.png")!)

        notUserBeaconsLabel.text          = "No stuff".localized
        notUserBeaconsLabel.textColor     = UIColor.whiteColor()
        notUserBeaconsLabel.textAlignment = NSTextAlignment.Center
        notUserBeaconsLabel.frame         = CGRectMake(0, 0, 250, 25)
        notUserBeaconsLabel.center        = CGPointMake(self.view.frame.size.width/2,self.view.frame.size.height/2)
        notUserBeaconsLabel.hidden        = false
        
        self.view.addSubview(notUserBeaconsLabel)
        userNotifier = UserNotifier(parentViewController: self, delegate: self)
        
        prepareBeaconsObservingValue()
    }
    
    func prepareBeaconsObservingValue() {
        for beacon in userBeacons {
            if let region = beacon.getBeaconRegion() {
                beacon.observing = beaconsFinder.lookingfForRegion(region)
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let beaconsCount = userBeacons.count
        if beaconsCount > 0 {
            notUserBeaconsLabel.hidden = true
        }
        else {
            notUserBeaconsLabel.hidden = false
        }
        return beaconsCount
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier("FindStuffCell", forIndexPath: indexPath)
        if let findStuffCell = cell as? FindStuffTableViewCell {
            let beacon = userBeacons[indexPath.row]
            
            findStuffCell.withStuffName( beacon.getName() )
                         .withDistance( beacon.getStringProximity() )
                         .withLocalStatus( beacon.isLocalNotificationEnabled() )
                         .withGlobalStatus( BeaconNotificationStatus.UNKNOWN )
            
            cell = findStuffCell as UITableViewCell
        }
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        selectedBeacon = userBeacons[indexPath.row]
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        userNotifier.notificationSelector().show()
    }
    
    func userSelectedLocalNotificationType(action: UIAlertAction){
        if let beacon = selectedBeacon{
            if let region = beacon.getBeaconRegion() {
                if !beaconsFinder.lookingfForRegion(region) {
                    beacon.observing = true
                    beaconsFinder.startRangingBeaconsInRegion(region)
                }
            }
        }
        tableView.reloadData()
    }
    
    func userSelectedGlobalNotificationType(action: UIAlertAction) {
        if let beacon = selectedBeacon {
            SwiftSpinner.show("Please wait".localized).addTapHandler({}, subtitle: "Adding your beacon to our looking for list".localized)
            POSTSender().addGlobalNotification(beacon).execute(globalNotificationResponse)
        }
    }
    
    func userSelectedDisableLocal(action: UIAlertAction) {
        if let beacon = selectedBeacon {
            if let region = beacon.getBeaconRegion() {
                if beaconsFinder.lookingfForRegion(region){
                    beacon.observing = false
                    beaconsFinder.stopRangingBeaconsInRegion(region)
                }
            }
        }
        tableView.reloadData()
    }
    
    func userSelectedDisableGlobal(action: UIAlertAction) {
        if let beacon = selectedBeacon {
            DELETESender().removeGlobalNotification(beacon).execute({_ in })
        }
    }
    
    func globalNotificationResponse(result: NSData?){
        if let notification = result {
            SwiftSpinner.hide()
            print(NSString(data: notification, encoding: NSUTF8StringEncoding)!)
        }
        dispatch_async(dispatch_get_main_queue()){
            SwiftSpinner.show(UserNotifier.CONNECTION_TIMEOUT_MESSAGE).addTapHandler({SwiftSpinner.hide()}, subtitle: "Tap to hide".localized)
        }
    }
    
    func finderDidRangeBeacons(beacons: [CLBeacon]){
        for beacon in beacons {
            for userBeacon in userBeacons {
                if userBeacon.isEqualToClBeacon(beacon){
                    updateCellWithUserBeacon(userBeacon, andProximity: beacon.proximity)
                }
            }
        }
    }

    func updateCellWithUserBeacon(beaconExt: BeaconExt, andProximity: CLProximity) {
        if let beaconIndex = userBeacons.indexOf(beaconExt) {
            userBeacons[beaconIndex].proximity = andProximity
            tableView.reloadData()
        }
    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        }
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
