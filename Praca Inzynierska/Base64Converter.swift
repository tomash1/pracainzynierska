//
//  Base64Converter.swift
//  Praca Inzynierska
//
//  Created by Tomek Domaracki on 24.09.2015.
//  Copyright (c) 2015 Tomek Domaracki. All rights reserved.
//

import UIKit

class Base64Converter: NSObject {
    
    func encodeString(toEncode : String?) -> String? {
        if let toEncode = toEncode {
            let utf8str = toEncode.dataUsingEncoding(NSUTF8StringEncoding)
            
            if let base64Encoded = utf8str?.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0)) {
                return base64Encoded
            }
        }
        return nil
    }
    
    func decodeString(toDecode : String?) -> String? {
        if let toDecode = toDecode {
            if let base64Decoded = NSData(base64EncodedString: toDecode, options: NSDataBase64DecodingOptions(rawValue: 0))
                .map({ NSString(data: $0, encoding: NSUTF8StringEncoding) }) {
                    
                return base64Decoded as String!
            }
        }
        return nil
    }
}
