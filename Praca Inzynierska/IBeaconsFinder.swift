//
//  IBeaconsFinder.swift
//  Praca Inzynierska
//
//  Created by Tomek Domaracki on 19.11.2015.
//  Copyright © 2015 Tomek Domaracki. All rights reserved.
//

import UIKit

protocol IBeaconsFinderDelegate {
    func finderDidRangeBeacons(beacons: [CLBeacon])
}

class IBeaconsFinder: NSObject, CLLocationManagerDelegate {

    var locationManager :CLLocationManager!
    var delegate: IBeaconsFinderDelegate? = nil
    
    override init(){
        locationManager = CLLocationManager()
        super.init()
        
        locationManager.delegate = self
    }
    
    func getRegions() -> Set<CLRegion> {
        return locationManager.monitoredRegions
    }
    
    func startRangingBeaconsInRegion(region: CLBeaconRegion) {
        if !locationManager.rangedRegions.contains(region) {
            locationManager.startRangingBeaconsInRegion(region)
        }
        if !locationManager.monitoredRegions.contains(region) {
            locationManager.startMonitoringForRegion(region)
        }
    }
    
    func stopRangingBeaconsInRegion(region: CLBeaconRegion) {
        if locationManager.monitoredRegions.contains(region){
            locationManager.stopMonitoringForRegion(region)
        }
        if locationManager.rangedRegions.contains(region){
            locationManager.stopRangingBeaconsInRegion(region)
        }
    }
    
    func stopLookingForAllBeacons() {
        for region in getRegions() {
            if let region = region as? CLBeaconRegion {
                stopRangingBeaconsInRegion(region)
            }
        }
    }
    
    func lookingfForRegion(region: CLRegion) -> Bool{
        return locationManager.monitoredRegions.contains(region)
    }
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        print("Location manager failed " + error.localizedDescription)
    }
    
    func locationManager(manager: CLLocationManager, monitoringDidFailForRegion region: CLRegion?, withError error: NSError) {
        print("Monitoring did fail for region " + error.localizedDescription)
    }
    
    func locationManager(manager: CLLocationManager, didRangeBeacons beacons: [CLBeacon], inRegion region: CLBeaconRegion) {
        if let configuredDelegate = delegate {
            configuredDelegate.finderDidRangeBeacons(beacons)
        }
    }
}
