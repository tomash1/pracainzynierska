//
//  BeaconRegionsFileManager.swift
//  Praca Inzynierska
//
//  Created by Tomek Domaracki on 09.11.2015.
//  Copyright © 2015 Tomek Domaracki. All rights reserved.
//

import UIKit

class BeaconRegionsFileManager: NSObject {
    let file : String
    var filePath = ""
    
    var userRegions: Array<CLBeaconRegion> = Array()
    var fileContent: String = ""
    
    override init() {
        let userId = UserCredentialsManager.getUserId()
        if let id = userId {
            file = "regions" + id + ".txt"
            
            super.init()
            if let dir : NSString = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true).first {
                filePath = dir.stringByAppendingPathComponent(file);
            }
            readFile()
        }
        else {
            file = ""
            super.init()
        }
    }
    
    private func readFile() {
        do{
            fileContent = try String(contentsOfFile: filePath, encoding: NSUTF8StringEncoding)
        } catch let error as NSError {
            print("Reading " + error.localizedDescription)
        }
        convertFileContentToUserRegions()
    }
    
    func addRegion(region: CLBeaconRegion) {
        userRegions.append(region)
        save()
    }
    
    func removeRegion(region: CLBeaconRegion) {
        if let index = userRegions.indexOf(region) {
            userRegions.removeAtIndex(index)
            save()
        }
    }
    
    func getRegions() -> Array<CLBeaconRegion>{
        return userRegions
    }
    
    func save(){
        fileContent = convertRegionsToString()
        if let outputStream = NSOutputStream(toFileAtPath: filePath, append: false) {
            outputStream.open()
            outputStream.write(fileContent, encoding: NSUTF8StringEncoding, allowLossyConversion: false)
            
            outputStream.close()
        } else {
            print("Saving: Unable to open file")
        }
    }
    
    private func convertRegionsToString() -> String {
        var regionsString = ""
        for region in userRegions{
            let regionString = convertRegionToString(region)
            regionsString += regionString + "\n"
        }
        return regionsString
    }
    
    private func convertRegionToString(regionToConvert: CLBeaconRegion) -> String {
        var regionString = ""
        regionString += regionToConvert.proximityUUID.UUIDString
        regionString += ";" + regionToConvert.major!.stringValue
        regionString += ";" + regionToConvert.minor!.stringValue
        regionString += ";" + regionToConvert.identifier
        return regionString
    }
    
    private func convertStringToRegion(stringToConvert: String) -> CLBeaconRegion? {
        let splittedString = stringToConvert.componentsSeparatedByString(";")
        if splittedString.count >= 3{
            let UUID        = NSUUID(UUIDString: splittedString[0])!
            let major       = Int(splittedString[1])!
            let minor       = Int(splittedString[2])!
            let identifier  = splittedString[3]
            
            let region = CLBeaconRegion(proximityUUID: UUID, major: UInt16(major), minor: UInt16(minor), identifier: identifier)
            return region
        }
        return nil
    }
    
    private func convertFileContentToUserRegions(){
        var regions: Array<CLBeaconRegion> = Array()
        if fileContent != "" {
            let splittedFileContent = fileContent.componentsSeparatedByString("\n")
            for fileElement in splittedFileContent{
                if fileElement == "" { continue }
                if let region = convertStringToRegion(fileElement) {
                    regions.append(region)
                }
            }
        }
        userRegions = regions
    }
}

extension NSOutputStream {
    
    /// Write String to outputStream
    ///
    /// - parameter string:                The string to write.
    /// - parameter encoding:              The NSStringEncoding to use when writing the string. This will default to UTF8.
    /// - parameter allowLossyConversion:  Whether to permit lossy conversion when writing the string.
    ///
    /// - returns:                         Return total number of bytes written upon success. Return -1 upon failure.
    
    func write(string: String, encoding: NSStringEncoding = NSUTF8StringEncoding, allowLossyConversion: Bool = true) -> Int {
        if let data = string.dataUsingEncoding(encoding, allowLossyConversion: allowLossyConversion) {
            var bytes = UnsafePointer<UInt8>(data.bytes)
            var bytesRemaining = data.length
            var totalBytesWritten = 0
            
            while bytesRemaining > 0 {
                let bytesWritten = self.write(bytes, maxLength: bytesRemaining)
                if bytesWritten < 0 {
                    return -1
                }
                
                bytesRemaining -= bytesWritten
                bytes += bytesWritten
                totalBytesWritten += bytesWritten
            }
            
            return totalBytesWritten
        }
        
        return -1
    }
    
}
