//
//  ConnectBeaconViewController.swift
//  Praca Inzynierska
//
//  Created by Tomasz Domaracki on 09.10.2015.
//  Copyright © 2015 Tomek Domaracki. All rights reserved.
//

import UIKit
import SwiftSpinner

protocol ConnectBeaconViewControllerDelegate {
    func connectedToBeacon(beacon: BeaconDeviceExt?)
}

class ConnectActionMessages: NSObject {
    static let Connecting       = "Connecting".localized
    static let Connected        = "Connected".localized
    static let Disconnected     = "Disconnected".localized
    static let Authenticating   = "Authenticating".localized
    static let Discovering      = "Discovering Services".localized
    static let Reading          = "Reading Beacon Data".localized
    static let Unknown          = "Unknown State".localized
}

class ConnectBeaconViewController: UIViewController, BLUConfigurableBeaconDelegate, UserNotifierDelegate {

    @IBOutlet weak var beaconNameLabel: UILabel!
    @IBOutlet weak var connectionStatusLabel: UILabel!
    @IBOutlet weak var connectionProgressView: UIProgressView!
    
    var selectedFoundBeacon : BLUBeacon?
    var selectedUserBeacon  : BeaconDeviceExt?
    var beaconManager       : BLUBeaconManager?
    var delegate            : ConnectBeaconViewControllerDelegate! = nil
    var unwindToViewController: String?
    
    var beaconDeviceExt  : BeaconDeviceExt!
    var connectionError  : NSError?
    var userNotifier     : UserNotifier?

    var waitingForUserReaction = false
    var userBeaconStatus = BeaconAvailability.Unknown
    
    var progressCounter : Int = 0 {
        didSet {
            let animated = progressCounter != 0
            connectionProgressView.setProgress(Float(progressCounter) / 100.0, animated: animated)
        }
    }
    
    func removeObserversForBeacon(beacon: BLUBeacon) {
        beacon.removeObserver(self, forKeyPath: "RSSI")
    }
    
    func addObserversForBeacon(beacon: BLUBeacon) {
        beacon.addObserver(self, forKeyPath: "RSSI", options: NSKeyValueObservingOptions.Prior, context: nil)
    }

    func addObserversForBeaconManager() {
        if let manager = beaconManager {
            manager.addObserver(self, forKeyPath: "centralManagerState", options: NSKeyValueObservingOptions.New, context: nil)
        }
    }
    
    func removeObserversForBeaconManager() {
        if let manager = beaconManager {
            manager.removeObserver(self, forKeyPath: "centralManagerState")
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        addObserversForBeaconManager()
    }
    
    func executeUnwindWhenUnsupportedBeacon(message: String) {
        self.connectionStatusLabel.text = message
        NSTimer.scheduledTimerWithTimeInterval(NSTimeInterval(3), target: self, selector: "performUnwind", userInfo: nil, repeats: false)
    }
    
    func performUnwind() {
        var unwindTo = "unwindToAddBeacons"
        if let unwindSelected = unwindToViewController {
            unwindTo = unwindSelected
        }
        
        performSegueWithIdentifier(unwindTo, sender: self)
    }
    
    func setupBeacon(beacon: BLUConfigurableBeacon) {
        let beaconDeviceExt = BeaconDeviceExt(beacon: beacon)
        beaconDeviceExt.BeaconDevice.delegate = self
        
        self.beaconDeviceExt      = beaconDeviceExt
        self.beaconNameLabel.text = beaconDeviceExt.deviceName
    }
    
    func setupBeacon(beaconDeviceExt: BeaconDeviceExt) -> Bool {
        if beaconDeviceExt.BeaconDevice != nil {
            beaconDeviceExt.BeaconDevice.delegate = self
            self.beaconDeviceExt = beaconDeviceExt
            beaconNameLabel.text = beaconDeviceExt.deviceName
            userBeaconStatus     = beaconDeviceExt.status
            return true
        }
        return false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let beacon = selectedFoundBeacon as? BLUConfigurableBeacon {
            setupBeacon(beacon)
            beaconDeviceExt.connect()
            return
        }
        else if let beacon = selectedUserBeacon {
            if setupBeacon(beacon) {
                beaconDeviceExt.connect()
                return
            }
        }
        executeUnwindWhenUnsupportedBeacon("Unsupported beacon selected".localized)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
        
    // MARK: - Beacon connection
    
    func configurableBeacon(configurableBeacon: BLUConfigurableBeacon, didChangeState state: BLUConfigurableBeaconConnectionState) {
        updateViewToConnectionState(configurableBeacon.connectionState)
    }
    
    func userAgreedWithWarningPopup(action : UIAlertAction) {
        self.connectionStatusLabel.text = ConnectActionMessages.Connecting
        self.waitingForUserReaction = false
        self.beaconDeviceExt.BeaconDevice.forceConnectWithPassword(nil, timeoutInterval: 20.0, retryCount: 3)
    }
    
    func userCanceledWarningPopup(action : UIAlertAction) {
        self.waitingForUserReaction = false
        self.executeUnwindWhenUnsupportedBeacon("Firmware update needed".localized)
    }
    
    func configurableBeacon(configurableBeacon: BLUConfigurableBeacon, didDisconnectWithError error: NSError?) {
        connectionProgressView.setProgress(0.0, animated: false)
        if let unWrappedError = error {
            connectionError             = unWrappedError
            connectionStatusLabel.text  = unWrappedError.localizedDescription
            
            if (unWrappedError.code == 5022) {
                waitingForUserReaction = true
                
                self.userNotifier = UserNotifier(parentViewController: self, delegate: self)
                self.userNotifier!.warningAgreeActionMessage = "Force Connect".localized
                self.userNotifier!.warningPopup(unWrappedError.localizedDescription).show()
            }
            else {
                if !waitingForUserReaction {
                    executeUnwindWhenUnsupportedBeacon(unWrappedError.localizedDescription)
                }
            }
        }
    }

    
    func updateViewToConnectionState(state: BLUConfigurableBeaconConnectionState) {
        var title = ""
        switch(state){
        case BLUConfigurableBeaconConnectionState.Connected:
            title = ConnectActionMessages.Connected
            break
        case BLUConfigurableBeaconConnectionState.Connecting:
            title = ConnectActionMessages.Connecting
            break
        case BLUConfigurableBeaconConnectionState.Authenticating:
            title = ConnectActionMessages.Authenticating
            break
        case BLUConfigurableBeaconConnectionState.Disconnected:
            title = ConnectActionMessages.Disconnected
            break
        case BLUConfigurableBeaconConnectionState.Discovering:
            title = ConnectActionMessages.Discovering
            break
        case BLUConfigurableBeaconConnectionState.Reading:
            title = ConnectActionMessages.Reading
        default:
            title = ConnectActionMessages.Unknown
        }
        
        progressCounter += 20
        self.connectionStatusLabel.text = title
    }
    
    // MARK: - Beacon connected
    
    func configurableBeaconDidConnect(configurableBeacon: BLUConfigurableBeacon) {
        configurableBeacon.enableBeaconSpeaker(false, andLED: true)
        
        beaconDeviceExt.beaconName = BLUBeaconExt(bluBeacon: configurableBeacon).getBeaconName()
        beaconDeviceExt.BeaconDevice = configurableBeacon
        updateViewToConnectionState(configurableBeacon.connectionState)
        
        if userBeaconStatus == BeaconAvailability.Disconnected {
            beaconConfiguredAlready()
        }
        else {
            SwiftSpinner.show("Beacon ready".localized, animated: false).addTapHandler({
                        SwiftSpinner.hide()
                        self.performSegueWithIdentifier("SaveConnectedBeaconSegue", sender: self)
            }, subtitle: "Tap to continue".localized)
        }
    }
    
    func beaconConfiguredAlready() {
        selectedUserBeacon!.status = BeaconAvailability.Connected
        delegate.connectedToBeacon(selectedUserBeacon!)
        performSegueWithIdentifier("unwindToYourBeacons", sender: self)
    }
    
    override func viewWillDisappear(animated: Bool) {
        removeObserversForBeaconManager()        
        super.viewWillDisappear(animated)
    }
    
    // MARK: - Key value observing
    
    override func observeValueForKeyPath(keyPath: String?, ofObject object: AnyObject?, change: [String : AnyObject]?, context: UnsafeMutablePointer<Void>) {
        
        if let path = keyPath {
            if path == "RSSI" {
                
            }
            else if path == "centralManagerState" {
                dispatch_async(dispatch_get_main_queue(), {
                    if self.beaconManager!.centralManagerState == CBCentralManagerState.PoweredOn{
                        self.connectionStatusLabel.text = "Powered On".localized
                    }
                    });
            }
        }
    }
    
    // MARK: - Navigation
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "SaveConnectedBeaconSegue" {
            if let destinationViewController = segue.destinationViewController as? SaveBeaconOnServerViewController {
                destinationViewController.beaconToSave = self.beaconDeviceExt
                destinationViewController.connectoToBeaconViewControllerWithSettedDelegate = self
            }
        }
    }
}
