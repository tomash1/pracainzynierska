//
//  GlobalNotificationsTableViewController.swift
//  Praca Inzynierska
//
//  Created by Tomek Domaracki on 23.11.2015.
//  Copyright © 2015 Tomek Domaracki. All rights reserved.
//

import UIKit
import SwiftyJSON
import SwiftSpinner

class GlobalNotificationsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet var tableView: GlobalNotificationsTableView!
    
    var userNotifications = Array<Notification>()
    let noNotificationsExistsLabel = UILabel()
    var selectedIndexPath: NSIndexPath!
    
    private func simulateNotification(){
        let notification = Notification()
        notification.geoCoordinatesLatitude  = MapKitDefaults.DEFAULT_LATITUDE
        notification.geoCoordinatesLongitude = MapKitDefaults.DEFAULT_LONGITUDE
        notification.message = "15.11.2015"
        
        userNotifications.append(notification)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.dataSource = self
        self.tableView.delegate = self
        //self.tableView.clipsToBounds  = false
        //self.tableView.backgroundView = UIImageView(image: UIImage(named: "app.png")!)

        noNotificationsExistsLabel.text          = "No notifications".localized
        noNotificationsExistsLabel.textColor     = UIColor.blackColor()
        noNotificationsExistsLabel.textAlignment = NSTextAlignment.Center
        noNotificationsExistsLabel.frame         = CGRectMake(0, 0, 300, 25)
        noNotificationsExistsLabel.center        = CGPointMake(self.view.frame.size.width/2,self.view.frame.size.height/2)
        noNotificationsExistsLabel.hidden        = false
        
        self.view.addSubview(noNotificationsExistsLabel)
        GETSender().userNotifications().execute(userNotificationResponse)
        SwiftSpinner.show("Getting notifications".localized)
    }

    func userNotificationResponse(result: NSData?){
        if let notificationsArrayRaw = result {
            let notificationsArrayJSON = JSON(data: notificationsArrayRaw, options: NSJSONReadingOptions.MutableContainers)
            let notificationsArray = JSONTranslator.convertJsonToUserNotificationsArray(notificationsArrayJSON)
            for notification in notificationsArray{
                userNotifications.append(notification)
            }
        }
        dispatch_async(dispatch_get_main_queue()){
            SwiftSpinner.hide()
            self.tableView.reloadData()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let notificationsCount = userNotifications.count
        if notificationsCount == 0 {
            noNotificationsExistsLabel.hidden = false
        } else {
            noNotificationsExistsLabel.hidden = true
        }
        return notificationsCount
    }

    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("NotificationCell", forIndexPath: indexPath)
        let notification = userNotifications[indexPath.row]
        
        cell.textLabel!.text        = notification.message
        cell.detailTextLabel!.text  = notification.timestamp
        return cell
    }


    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.tableView.deselectRowAtIndexPath(indexPath, animated: true)
        selectedIndexPath = indexPath
        performSegueWithIdentifier("ShowNotificationMapView", sender: self)
    }
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            let notification = userNotifications[indexPath.row]
            userNotifications.removeAtIndex(indexPath.row)
            
            DELETESender().deleteNotification(notification).execute({_ in})
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        }
    }


    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */


    // MARK: - Navigation
    
    @IBAction func prepareForUnwindToGlobalNotifications(segue: UIStoryboardSegue){}
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "ShowNotificationMapView" {
            if let destinationViewController = segue.destinationViewController as? NotificationMapViewController {
                destinationViewController.notification = userNotifications[selectedIndexPath.row]
            }
        }
    }


}
