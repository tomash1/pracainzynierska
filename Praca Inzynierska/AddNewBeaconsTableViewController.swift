//
//  AddNewBeaconsTableViewController.swift
//  Praca Inzynierska
//
//  Created by Tomasz Domaracki on 09.10.2015.
//  Copyright © 2015 Tomek Domaracki. All rights reserved.
//

import UIKit

protocol AddNewBeaconsTableViewControllerDelegate {
    func addNewBeaconsDidConnectToBeacon(beacon: BeaconDeviceExt)
}

class AddNewBeaconsTableViewController: UITableViewController, BLUBeaconManagerDelegate, ConnectBeaconViewControllerDelegate {
    @IBOutlet var addRegionManuallyButton: UIBarButtonItem!
    
    var beaconManager : BLUBeaconManager!
    
    var foundBeacons = NSMutableArray()
    
    var selectedIndexPath : NSIndexPath?
    var delegate: AddNewBeaconsTableViewControllerDelegate! = nil
    
    let lookingForBeaconsMessageLabel = UILabel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        beaconManager = BLUBeaconManager(delegate: self)

        tableView.clipsToBounds = false
        tableView.backgroundView = UIImageView(image: UIImage(named: "app.png")!)

        initLookingForBeaconsMessage()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        if !beaconManager.scanning{
            beaconManager.startScanningForBeacons()
        }
    }
    
    func initLookingForBeaconsMessage() {
        lookingForBeaconsMessageLabel.text          = "Looking for beacons".localized
        lookingForBeaconsMessageLabel.textColor     = UIColor.whiteColor()
        lookingForBeaconsMessageLabel.textAlignment = NSTextAlignment.Center
        lookingForBeaconsMessageLabel.frame         = CGRectMake(0, 0, 250, 25)
        lookingForBeaconsMessageLabel.center        = CGPointMake(self.view.frame.size.width/2,self.view.frame.size.height/2)
        lookingForBeaconsMessageLabel.hidden        = false
        
        self.view.addSubview(lookingForBeaconsMessageLabel)
    }
    
    func showLookingForBeaconsMessage() {
        lookingForBeaconsMessageLabel.hidden = false
    }
    
    func hideLookingForBeaconsMessage() {
        lookingForBeaconsMessageLabel.hidden = true
    }
    
    func addfoundBeacon(beacon : BLUBeacon){
        foundBeacons.addObject(beacon)
        let indexPath = NSIndexPath(forRow: self.foundBeacons.count - 1, inSection: 0)

        self.tableView.insertRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
    }
    
    func removeFoundBeacon(beacon : BLUBeacon){
        self.foundBeacons.removeObject(beacon)
        self.tableView.reloadData()
    }
    
    func connectedToBeacon(beacon: BeaconDeviceExt?) {
        if let connectedBeacon = beacon {
            foundBeacons.removeObject(connectedBeacon.BeaconDevice as BLUBeacon)
            tableView.reloadData()
            if self.delegate != nil {
                self.delegate.addNewBeaconsDidConnectToBeacon(connectedBeacon)
            }
        }
    }
    
    // MARK: - Core Bluetooth
    
    func beaconManager(manager: BLUBeaconManager, didFindBeacon beacon: BLUBeacon) {
        addfoundBeacon(beacon)
    }
    
    func beaconManager(manager: BLUBeaconManager, didLoseBeacon beacon: BLUBeacon) {
        removeFoundBeacon(beacon)
    }
    

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if foundBeacons.count == 0 {
            showLookingForBeaconsMessage()
        } else {
            hideLookingForBeaconsMessage()
        }
        return foundBeacons.count
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        selectedIndexPath = indexPath
        performSegueWithIdentifier("ConnectToFoundBeaconSegue", sender: nil)
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("BeaconCell", forIndexPath: indexPath)
        
        if let cell = cell as? BeaconTableViewCell {
            let selectedBeacon = foundBeacons[indexPath.row]
            if let bluBeacon = selectedBeacon as? BLUBeacon {
                let beaconExt = BLUBeaconExt(bluBeacon: bluBeacon)
                
                cell.withTitle( beaconExt.getBeaconName() )
                    .withRssiValue( beaconExt.getStrength() )
            }
        }
        return cell
    }

    @IBAction func addRegionManuallyButtonClicked(sender: AnyObject) {
        print("clicked")
    }
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */
    
    // MARK: - Navigation
    
    @IBAction func prepareForUnwindToAddNewBeacons(segue : UIStoryboardSegue) {}
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "ConnectToFoundBeaconSegue" {
            if let destinationViewControler = segue.destinationViewController as? ConnectBeaconViewController {
                let beacon = foundBeacons.objectAtIndex(self.selectedIndexPath!.row) as! BLUBeacon
                
                beaconManager.stopScanning()
                
                destinationViewControler.selectedFoundBeacon = beacon
                destinationViewControler.beaconManager = beaconManager
                destinationViewControler.unwindToViewController = "unwindToAddBeacons"
                destinationViewControler.delegate = self
            }
        }
    }
    
}
