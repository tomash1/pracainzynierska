//
//  UserNotifier.swift
//  Praca Inzynierska
//
//  Created by Tomasz Domaracki on 29.09.2015.
//  Copyright © 2015 Tomek Domaracki. All rights reserved.
//

import UIKit

@objc protocol UserNotifierDelegate {
    
    optional func userAgreedWithWarningPopup(action : UIAlertAction)
    
    optional func userCanceledWarningPopup(action : UIAlertAction)
    
    optional func userReadedInfoMessage(action: UIAlertAction)
    
    optional func userSelectedLocalNotificationType(action: UIAlertAction)

    optional func userSelectedGlobalNotificationType(action: UIAlertAction)
    
    optional func userSelectedDisableLocal(action: UIAlertAction)
    
    optional func userSelectedDisableGlobal(action: UIAlertAction)
    
    optional func userCanceledNotificationSelector(action: UIAlertAction)
}

class UserNotifier: NSObject {

    static let AUTH_MESSAGE = "Logging".localized
    static let WRONG_CREDENTIALS_MESSAGE = "Wrong Credentials".localized
    static let CONNECTION_TIMEOUT_MESSAGE = "Connection Error".localized
    
    var infoAgreeActionMessage = "OK".localized
    var warningAgreeActionMessage = "OK".localized
    var cancelActionMessage = "Cancel".localized
    
    var controllerToShow: UIAlertController? = nil
    
    let parentViewController: UIViewController!
    private let delegate: UserNotifierDelegate!
    
    init(parentViewController: UIViewController, delegate: UserNotifierDelegate) {
        self.parentViewController = parentViewController
        self.delegate = delegate
    }
    
    func notificationSelector() -> UserNotifier {
        if let delegateObject = delegate {
            let notifyController = UIAlertController(title: "Select notification".localized, message: "", preferredStyle: .ActionSheet)
            
            let localRangingAction = UIAlertAction(title: "Local".localized, style: .Default, handler: delegateObject.userSelectedLocalNotificationType)
            
            let globalRangingAction = UIAlertAction(title: "Global".localized, style: .Default, handler: delegateObject.userSelectedGlobalNotificationType)
            
            let disableLocal = UIAlertAction(title: "Disable local".localized, style: .Destructive, handler: delegateObject.userSelectedDisableLocal)
            
            let disableGlobal = UIAlertAction(title: "Disable global".localized, style: .Destructive, handler: delegateObject.userSelectedDisableGlobal)
            
            let cancel = UIAlertAction(title: "Cancel".localized, style: .Cancel, handler: delegateObject.userCanceledNotificationSelector)
            
            notifyController.addAction(localRangingAction)
            notifyController.addAction(globalRangingAction)
            notifyController.addAction(disableLocal)
            notifyController.addAction(disableGlobal)
            notifyController.addAction(cancel)
            
            controllerToShow = notifyController
        }
        return self
    }
    
    func infoPopup(message: String) -> UserNotifier {
        if let delegateObject = delegate {
            let infoController = UIAlertController(title: "Info".localized, message: message, preferredStyle: UIAlertControllerStyle.Alert)
            
            let okAction = UIAlertAction(title: infoAgreeActionMessage, style: UIAlertActionStyle.Default, handler: delegateObject.userReadedInfoMessage)
            
            infoController.addAction(okAction)
            
            controllerToShow = infoController
        }
        return self
    }
    
    func warningPopup(message: String) -> UserNotifier {
        if let delegateObject = delegate {
            let alertController = UIAlertController(title: "Warning".localized, message: message, preferredStyle: UIAlertControllerStyle.Alert)
            
            let agreeAction = UIAlertAction(title: warningAgreeActionMessage, style: UIAlertActionStyle.Default, handler: delegateObject.userAgreedWithWarningPopup)
            let cancelAction = UIAlertAction(title: cancelActionMessage, style: UIAlertActionStyle.Cancel, handler: delegateObject.userCanceledWarningPopup)
            
            alertController.addAction(agreeAction)
            alertController.addAction(cancelAction)
            
            controllerToShow = alertController
        }
        return self
    }
    
    func show() {
        if let controller = controllerToShow {
            parentViewController.presentViewController(controller, animated: true, completion: nil)
        }
    }
}
