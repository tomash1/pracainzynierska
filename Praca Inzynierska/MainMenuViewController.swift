//
//  MainMenuViewController.swift
//  Praca Inzynierska
//
//  Created by Tomek Domaracki on 30.06.2015.
//  Copyright (c) 2015 Tomek Domaracki. All rights reserved.
//

import UIKit
import SwiftSpinner
import SwiftyJSON

class MainMenuViewController: UIViewController, YourBeaconsDelegate {

    @IBOutlet var rightButtonFromNavigationControllerBar: UIBarButtonItem!
    @IBOutlet var leftButtonFromNavigationControllerBar: UIBarButtonItem!
    
    var userBeacons : Array<BeaconDeviceExt> = Array()
    var reloadDataFromServer = true
    var global: GlobalNotificationsHandler = GlobalNotificationsHandler()
    
    @IBAction func buttonClicked(sender: AnyObject) {
        let testRegion = CLBeaconRegion(proximityUUID: NSUUID(UUIDString: "d8cfa7a5-f3d8-47a0-b25e-92dcbe690e06")!, major: 8, minor: 1, identifier: "bag2")
        global.handleNotification(testRegion, isEnterNotification: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
     
        let navBarLook = NavigationBarLook(controller: navigationController!)
        navBarLook.setLook()
        navBarLook.addLogoToBarTitle(navigationItem)
        
        navigationController!.reloadInputViews()
//        navBarLook.addShadowToBarButton(rightButtonFromNavigationControllerBar)
//        navBarLook.addShadowToBarButton(leftButtonFromNavigationControllerBar)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        if reloadDataFromServer {
            reloadDataFromServer = false
            SwiftSpinner.show("Getting things ready".localized).addTapHandler({}, subtitle: "Please wait".localized)
            getDataFromServer()
        }
    }
    
    private func getDataFromServer(){
        let app = UIApplication.sharedApplication().delegate as! AppDelegate
        if let deviceId = app.deviceUUID {
            let userDevice = Device()
            userDevice.deviceId = deviceId
            POSTSender().addDevice(userDevice).execute { (result) -> Void in }
        }
        GETSender().beacons().execute(userBeaconsResponseReceived)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func LogOutButtonClicked(sender: AnyObject) {
        UserCredentialsManager.removeUserData()
        IBeaconsFinder().stopLookingForAllBeacons()
        
        let navigator = StoryboardNavigator(storyboardName: "Main")
        navigator.moveToStoryboard(self)
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return UIStatusBarStyle.LightContent
    }
    
    func globalNotificationsResponseReceived(result: NSData?) {
        NotificationsHandler().handleGlobalNotificationsResponse(result)
        SwiftSpinner.hide()
    }
    
    func userBeaconsResponseReceived(result: NSData?) {
        if let beaconsArrayRaw = result {
            let beaconsArrayJSON = JSON(data: beaconsArrayRaw, options: NSJSONReadingOptions.MutableContainers)
            let beaconsArray = JSONTranslator.convertJsonToBeaconsArray(beaconsArrayJSON)
        
            for beacon in beaconsArray {
                let beaconDevice = BeaconDeviceExt(beacon: beacon)
                beaconDevice.status = BeaconAvailability.Disconnected
                beaconDevice.beaconName = beacon.name
                self.userBeacons.append(beaconDevice)
            }
        }
        GETSender().globalNotifications().execute(globalNotificationsResponseReceived)
    }
    
    
    func newBeaconAdded(beacon: BeaconDeviceExt) {
        userBeacons.append(beacon)
    }
    
    func beaconRemoved(beacon: BeaconDeviceExt) {
        if let index = userBeacons.indexOf({$0.id == beacon.id}){
            userBeacons.removeAtIndex(index)
        }
    }
    
    private func isUserBeaconRegion(region: CLBeaconRegion) -> Bool {
        for beaconDev in userBeacons {
            let beaconExt = BeaconExt(beaconDeviceExt: beaconDev)
            if beaconExt.getBeaconRegion() == region {
                return true
            }
        }
        return false
    }
    
    @IBAction func prepareForUnwindToMainManu(seguu: UIStoryboardSegue){}
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "ManageBeaconsSegue" {
            if let manageBeaconsViewController = segue.destinationViewController as? ManageBeaconsViewController {
                manageBeaconsViewController.userBeacons = userBeacons
                manageBeaconsViewController.yourBeaconDelegate = self
            }
        }
        else if segue.identifier == "FindStuffSegue" {
            if let findStuffTableViewController = segue.destinationViewController as? FindStuffTableViewController {
                var userBeaconsExt: Array<BeaconExt> = Array()
                for beacon in userBeacons {
                    userBeaconsExt.append(BeaconExt(beaconDeviceExt: beacon))
                }
                findStuffTableViewController.userBeacons = userBeaconsExt
            }
        }
        else if segue.identifier == "RadarSegue" {
            if let radarTableViewController = segue.destinationViewController as? RadarTableViewController {
                radarTableViewController.userBeacons = userBeacons
            }
        }
    }
}
