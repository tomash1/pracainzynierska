//
//  ViewController.swift
//  Praca Inzynierska
//
//  Created by Tomek Domaracki on 28.06.2015.
//  Copyright (c) 2015 Tomek Domaracki. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    
    @IBOutlet var SignInButton: UIButton!

    let animator = Animator()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let signInButton = ButtonLook(fromUIButton: SignInButton)
        signInButton.addBorderToButton()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        if UserCredentialsManager.userLoggedPreviously() {
            moveToMainMenuStoryboard()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func prepareForUnwind(segue : UIStoryboardSegue) {}
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let toViewController = segue.destinationViewController
        toViewController.transitioningDelegate = animator
    }
    
    func moveToMainMenuStoryboard(){
        let navigator = StoryboardNavigator(storyboardName: "LogedUserStory")
        navigator.moveToStoryboard(self)
    }
}

