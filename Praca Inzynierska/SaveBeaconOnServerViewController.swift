//
//  SaveBeaconOnServerViewController.swift
//  Praca Inzynierska
//
//  Created by Tomasz Domaracki on 14.10.2015.
//  Copyright © 2015 Tomek Domaracki. All rights reserved.
//

import UIKit
import SwiftSpinner

class SaveBeaconOnServerViewController: UIViewController {

    @IBOutlet weak var saveBeaconButton: UIButton!
    @IBOutlet weak var beaconNameTextField: UITextField!
    
    var beaconToSave: BeaconDeviceExt! = nil
    
    var beaconName = ""
    var beaconFromDb: Beacon!
    
    var connectoToBeaconViewControllerWithSettedDelegate: ConnectBeaconViewController! = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let buttonLook = ButtonLook(fromUIButton: saveBeaconButton)
        buttonLook.addRadiusToButton()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    // MARK: - Saving beacon in database
    
    func nameTextFieldIsEmpty() -> Bool {
        if let name = beaconNameTextField.text {
            if name != "" {
                beaconName  = name
                return false
            }
        }
        return true
    }
    
    func writeConfiguration(beacon: Beacon) -> Bool {
        if !beaconToSave.isConnected() { return false }
        
        if let UUID  = NSUUID(UUIDString: beacon.uuid!) {
            if let major = Int(beacon.majorId!) {
                if let minor = Int(beacon.minorId!) {
                    let beaconExt = BeaconExt(beacon: beacon)
                    if let region = beaconExt.getBeaconRegion() {
                        BeaconRegionsFileManager().addRegion(region)
                    }
                    
                    let configuration = beaconToSave.BeaconDevice.configuration.copy() as! BLUBeaconConfiguration
                    let iBeacon = BLUIBeacon(proximityUUID: UUID, major: major, minor: minor)
                    configuration.iBeacon = iBeacon

                    let advanced = BLUBeaconAdvancedSettings()
                    advanced.energySavingAdvertisementInterval = 0
                    advanced.energySavingTransmissionPower = -50
                    advanced.advertisementInterval = 1
                    advanced.transmissionPower = -4
                    configuration.iBeaconAdvancedSettings = advanced
                    
                    let confBeacon = beaconToSave.BeaconDevice
                    confBeacon.setConfiguration(configuration, options: BLUBeaconConfigurationOptions.ChangedValues, completion: { (writtenConf, error) -> Void in
                        self.successEndOfConfigurationWriting(writtenConf, error: error)
                    })
                }
            }
        }
        return true
    }
    
    func successEndOfConfigurationWriting(writtenConfiguration: BLUBeaconConfiguration?, error: NSError?) -> Void {
        var message = ""
        if let writeError = error {
            print((writeError as NSError).localizedDescription)
            message = "Writing configuration error".localized
        }
        else {
            message = "Success".localized
            
            let device = beaconToSave.BeaconDevice
            beaconToSave = BeaconDeviceExt(beacon: beaconFromDb)
            beaconToSave.status = BeaconAvailability.Connected
            beaconToSave.BeaconDevice = device
        }
        dispatch_async(dispatch_get_main_queue()) {
            SwiftSpinner.show(message, animated: false).addTapHandler(
                {
                    SwiftSpinner.hide()
                    self.connectoToBeaconViewControllerWithSettedDelegate.delegate.connectedToBeacon(self.beaconToSave)
                    self.performSegueWithIdentifier("unwindToAddBeacons", sender: self)
                    
                },
                subtitle: "Tap to hide".localized )
        }
    }
    
    @IBAction func saveBeaconToDbButtonClicked(sender: AnyObject) {
        self.view.endEditing(true)
        
        if nameTextFieldIsEmpty() {
            let animator = Animator()
            animator.animateTextFieldError(beaconNameTextField)
        }
        else {
            beaconToSave.status = BeaconAvailability.NotConfigured
            
            SwiftSpinner.show("Getting your credentials".localized)
            GETSender().beaconsCredentials().execute(beaconCredentialReceived)
        }
    }
    
    func beaconResponseReceived(result: NSData?) {
        let beaconFromJSON = Beacon.fromJSON(result)
        var message = ""
        var waitingForConfigurationToWrite = false
        if let beacon = beaconFromJSON {
            if !writeConfiguration(beacon) {
                message = "Beacon is disconnected".localized
            }
            else {
                beaconFromDb = beacon
                message = "Writing configuration".localized
                waitingForConfigurationToWrite = true
            }
        }
        else {
            message = UserNotifier.CONNECTION_TIMEOUT_MESSAGE
        }
        if !waitingForConfigurationToWrite {
            dispatch_async(dispatch_get_main_queue()) {
                SwiftSpinner.show(message, animated: false).addTapHandler(
                    {
                        SwiftSpinner.hide()
                        self.connectoToBeaconViewControllerWithSettedDelegate.delegate.connectedToBeacon(self.beaconToSave)
                        self.performSegueWithIdentifier("unwindToAddBeacons", sender: self)
                        
                    },
                    subtitle: "Tap to hide".localized )
            }
        }
    }
    
    func beaconCredentialReceived(result: NSData?) {
        let beaconCredentialFromJSON = BeaconCredential.fromJSON(result)
        if let beaconCredential = beaconCredentialFromJSON {
            let beacon = prepareBeacon(beaconCredential)
            dispatch_async(dispatch_get_main_queue()) {
                SwiftSpinner.show("Saving your beacon".localized)
            }
            POSTSender().addBeacon(beacon).execute(beaconResponseReceived)
        }
        else {
            dispatch_async(dispatch_get_main_queue()) {
                SwiftSpinner.show(UserNotifier.CONNECTION_TIMEOUT_MESSAGE, animated: false).addTapHandler(
                    {
                        SwiftSpinner.hide()
                        self.connectoToBeaconViewControllerWithSettedDelegate.delegate.connectedToBeacon(self.beaconToSave)
                        self.performSegueWithIdentifier("unwindToAddBeacons", sender: self)
                    },
                    subtitle: "Tap to hide".localized )
            }
        }
    }

    func prepareBeacon(credential: BeaconCredential) -> Beacon {
        let beacon      = Beacon()
        beacon.name     = self.beaconName
        beacon.uuid     = credential.uuid!
        beacon.majorId  = credential.majorId!
        beacon.minorId  = credential.minorId!
        beacon.deviceId = 1
        return beacon
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
