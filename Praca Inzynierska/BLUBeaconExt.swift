//
//  BeaconDelegate.swift
//  Praca Inzynierska
//
//  Created by Tomasz Domaracki on 08.10.2015.
//  Copyright © 2015 Tomek Domaracki. All rights reserved.
//

import UIKit

class BeaconAvailability {
 
    static let Connected        = "Connected".localized
    static let Disconnected     = "Disconnected".localized
    static let NotConfigured    = "Not Configured".localized
    static let DisconnectedNC   = "Disconnected/NC".localized
    static let Unknown          = "Unknown".localized
    
}

class BLUBeaconExt: NSObject {
    var _Beacon: BLUBeacon
    
    init (bluBeacon: BLUBeacon) {
        _Beacon = bluBeacon
    }
    
    init(bluBeaconExt: BLUBeaconExt) {
        _Beacon = bluBeaconExt._Beacon
    }
    
    func getBeaconName() -> String {
        if let bluBeacon = _Beacon as? BLUIBeacon {
            return bluBeacon.proximityUUID.UUIDString
        }
        else {
            let bluBeacon = _Beacon as! BLUBluetoothBeacon
            return bluBeacon.name
        }
    }
    
    func getBeaconStatus() -> String {
        if let configurableBeacon = _Beacon as? BLUConfigurableBeacon {
            let connectionState = configurableBeacon.connectionState
            switch(connectionState){
            case (BLUConfigurableBeaconConnectionState.Connected):
                return BeaconAvailability.Connected
            case (BLUConfigurableBeaconConnectionState.Disconnected):
                return BeaconAvailability.Disconnected
            default:
                return BeaconAvailability.Unknown
            }
        }
        else {
            return BeaconAvailability.Unknown
        }
    }
    
    func getStrength() -> String {
        let rssi = _Beacon.RSSI
        return String( rssi )
    }
}
